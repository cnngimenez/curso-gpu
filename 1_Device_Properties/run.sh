#! /bin/sh

# Enqueue for all groups
alias mrun='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-26] --gres=gpu:1'
# Enqueue for Group G
alias mrun_g='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-28] --gres=gpu:1'
# Enqueue for Group T
alias mrun_t='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[01-26] --gres=gpu:1'

# Parameters:
#
#     runall command parameters number-test
#
function runall {
    mrun_g $1 $2 > "results/$1-G-none-$3.out" 2> "results/$1-G-none-$3.err" &

    mrun_t $1 $2 > "results/$1-T-none-$3.out" 2> "results/$1-T-none-$3.err" & 
}

runall "device_properties" "" 1
