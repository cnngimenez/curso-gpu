reset

set title "Suma de Vectores: GPU Tesla vs CPU"
set title "Suma de Vectores: GPU GeForce vs CPU"
set xrange [0:22]

$labels <<EOD
x y text
4  0.01 "Veces = 1.\nDatos en aumento."
8  0.01 "100 datos.\nVeces en aumento."
11 0.01 "10000000 datos.\nVeces en aumento."
14 0.01 "Bloques en aumento."
18 0.01 "Repetir experimentos.\nBloques = 1024."
EOD

set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 6.5, graph 1
set obj rect from 9.5, graph 0 to 12.5, graph 1
set obj rect from 15.5, graph 0 to 19.5, graph 1

set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
set logscale y

set style fill solid 0.5
set style fill transparent

plot \
"results-t.csv" using "num":"gpu" with lines, \
"results-t.csv" using "num":"gpu" notitle with points, \
"results-t.csv" using "num":"cpu" with lines, \
"results-t.csv" using "num":"cpu" notitle with points, \
$labels using "x":"y":"text" notitle with labels;
pause 100
