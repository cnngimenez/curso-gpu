#! /usr/bin/fish

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    echo -n " "
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G"
set_color normal

echo "num tam veces cpu gpu HtoD DtoH memcpy" > results-g.csv
for fout in ../results/suma_vectores-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-g.csv
end

set_color --bold 
echo "Group T"
set_color normal

echo "num tam veces cpu gpu HtoD DtoH memcpy" > results-t.csv
for fout in ../results/suma_vectores-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-t.csv
end
