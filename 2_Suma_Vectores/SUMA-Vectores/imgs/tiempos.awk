BEGIN {
    ORS=""
}

/Tamaño del arreglo: .*/ {
    match ($0, "[[:digit:]]+", a)
    salida[0] = a[0]
}

/Veces: .*/ {
    match ($0, "[[:digit:]]+", a)
    salida[1] =  a[0]
}

/Tiempo en CPU: .*/ {
    match ($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]])", a)    
    salida[2] = a[0]
}

/Tiempo en GPU: .*/ {
    match ($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]])", a)
    salida[3] = a[0]
}

ENDFILE {
    match(ARGV[ARGIND], "[[:digit:]]+", a)
    print a[0] " "
    print salida[0] " " salida[1] " "
    print salida[2] " " salida[3] 
}
