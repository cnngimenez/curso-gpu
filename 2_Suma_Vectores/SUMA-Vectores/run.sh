#! /bin/sh

# Enqueue for all groups
alias mrun='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-26] --gres=gpu:1'
# Enqueue for Group G
alias mrun_g='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-28] --gres=gpu:1'
# Enqueue for Group T
alias mrun_t='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[01-26] --gres=gpu:1'

# Parameters:
#
#     runall command parameters number-test
#
function runall {
    mrun_g nvprof --metrics all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-metrics-$3.err" &
    mrun_g nvprof --events all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-events-$3.err" &
    mrun_g nvprof ./$1 $2 > "results/$1-G-summary-$3.out" 2> "results/$1-G-summary-$3.err" &
    
    
    mrun_t nvprof --metrics all ./$1 $2 > "results/$1-T-metrics-$3.out" 2> "results/$1-T-metrics-$3.err" &
    mrun_t nvprof --events all ./$1 $2 > "results/$1-T-events-$3.out" 2> "results/$1-T-events-$3.err" &
    mrun_t nvprof ./$1 $2 > "results/$1-T-summary-$3.out" 2> "results/$1-T-summary-$3.err" &
    
}

# The grid layout, if 1D, should be (amount_data + nThreads.x - 1) / nThreads.x

# Sinopsis del comando: suma_vectores tam veces gridx gridy blockx blocky
runall "suma_vectores" "100 1 1 1 256 1" 1
runall "suma_vectores" "1000 1 4 1 256 1" 2
runall "suma_vectores" "10000 1 40 1 256 1" 3
runall "suma_vectores" "100000 1 391 1 256 1" 4
runall "suma_vectores" "1000000 1 3907 1 256 1" 5
runall "suma_vectores" "10000000 1 39063 1 256 1" 6
runall "suma_vectores" "100 100 1 1 256 1" 7
runall "suma_vectores" "100 200 1 1 256 1" 8
runall "suma_vectores" "100 300 1 1 256 1" 10
runall "suma_vectores" "10000000 100 39063 1 256 1" 10
runall "suma_vectores" "10000000 200 39063 1 256 1" 11
runall "suma_vectores" "10000000 300 39063 1 256 1" 12

runall "suma_vectores" "10000000 300 19532 1 512 1" 13
runall "suma_vectores" "10000000 300 13021 1 768 1" 14
runall "suma_vectores" "10000000 300 9766 1 1024 1" 15

runall "suma_vectores" "1000 1 1 1 1024 1" 16
runall "suma_vectores" "10000 1 10 1 1024 1" 17
runall "suma_vectores" "100000 1 98 1 1024 1" 18
runall "suma_vectores" "1000000 1 977 1 1024 1" 19

runall "suma_vectores" "10000000 100 9766 1 1024 1" 20
runall "suma_vectores" "10000000 200 9766 1 1024 1" 21

