#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <cuda.h>

#include "../../common/cpu_timer.h"
#include "../../common/gpu_timer.h"
#include "../../common/check.h"

#include "vector_io.h"
#include "vector_ops.h"


int suma_secuencial(float *h_A, float *h_B, int size, int veces);
int suma_paralela(float *d_A, float *d_B, int size, int veces,
		  int blockx, int blocky, int gridx, int gridy );

int main(int argc, char **argv)
{

  if (argc < 6){
    printf("Synopsis: ./suma_vectores tam veces gridx gridy blockx blocky");
    exit(1);
  }
  const int tam   = atoi(argv[1]);
  const int veces = atoi(argv[2]);
  const int gridx = atoi(argv[3]);
  const int gridy = atoi(argv[4]);
  const int blockx = atoi(argv[5]);
  const int blocky = atoi(argv[6]);

  printf("Tamaño del arreglo: %d\nVeces: %d\n", tam, veces);
  printf("Grilla de %d x %d bloques\n", gridx, gridy);
  printf("Bloque de %d x %d threads\n", blockx, blocky);
  
  /* detecto placa y su nombre */
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, 0);
  printf("Computer name: %s \n ", deviceProp.name);


  /* reservar memoria en host para los vectores h_A, h_B, y h_aux */
  /* alocacion de memoria en host */
  float *h_A, *h_B, *h_aux;
  h_A = (float *) malloc(sizeof(float) * tam);
  h_B = (float *) malloc(sizeof(float) * tam);
  h_aux = (float *) malloc(sizeof(float) * tam);


  /* reservar memoria en device para d_A y d_B */
  /* alocacion de memoria en device */
  float *d_A, *d_B;
  cudaMalloc((void **) &d_A, sizeof(float) * tam);
  cudaMalloc((void **) &d_B, sizeof(float) * tam);    
  
  /* chequeo de alocacion de memoria */
  if (!h_A || !h_B || !d_A || !d_B || !h_aux) {
    printf("Error alocando vectores \n");
    exit(-1);
  }

  /* inicializacion de vectores */
  printf("Inicializacion vector A \n");
  vector_io_initializeRND(h_A, tam);
  printf("Inicializacion vector B \n");
  vector_io_initializeRND(h_B, tam);

 
  /* transferencia de datos cpu -> gpu (host -> device) */
  cudaMemcpy(d_A, h_A, sizeof(float) * tam, cudaMemcpyHostToDevice);
  check_errors("d_A <- h_A memcpy");
  cudaMemcpy(d_B, h_B, sizeof(float) * tam, cudaMemcpyHostToDevice);
  check_errors("d_B <- h_B memcpy");


  /* -- suma secuencial */ 
  printf("Suma secuencial (CPU)\n");

  cpu_timer crono_cpu;
  crono_cpu.tic();
    
  suma_secuencial(h_A, h_B, tam, veces);

  crono_cpu.tac();

    
  /* -- suma paralela */
  printf("Suma paralela (GPU) \n");
  suma_paralela(d_A, d_B, tam,
		veces,
		blockx, blocky, gridx, gridy);

    
  /* -- transferencia de datos desde GPU a CPU para testear la suma */
  cudaMemcpy(h_aux, d_A, sizeof(float) * tam, cudaMemcpyDeviceToHost);
  check_errors("h_aux <- d_A memcpy");

  /* se chequea el ultimo resultado, despues de sumar VECES veces*/
  if(vector_ops_iguales(h_aux, h_A, tam)) 
    printf("Test pasado! \n");
  else
    printf("Test no pasado! \n");


  /* liberar memoria en host */      
  /* liberacion de memoria */
  free(h_A);
  free(h_B);
  free(h_aux);    

  /* liberar memoria en device */      
  /* liberacion de memoria en device*/
  cudaFree(d_A);
  cudaFree(d_B);
    
  check_errors("cudaFree");
    
  return 0;
}



int suma_secuencial(float *h_A, float *h_B, int size,
		    int veces)
{
    
  /* Tomar el tiempo inicial */
  struct timeval start;
  gettimeofday(&start, NULL);

    
  int i;
  for(i = 0; i < veces; i++)
    {
      vector_ops_suma_sec(h_A, h_B, size);
    }

  /* tomar el tiempo final */
  struct timeval finish;
  gettimeofday(&finish, NULL);

  /* imprimir el tiempo transcurrido */
  double time = ((finish.tv_sec - start.tv_sec) * 1000.0) + ((finish.tv_usec - start.tv_usec) / 1000.0);
  printf("Tiempo en CPU: %g ms \n", time);


  return 1;
}


int suma_paralela(float *d_A, float *d_B, int size,
		  int veces,
		  int blockx, int blocky, int gridx, int gridy)
{ 
   
  /* variables para tomar el tiempo en GPU: events */
  cudaEvent_t start, stop;
  float elapsedTime;
    
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
    
  /* tomar el tiempo inicial */
  cudaEventRecord(start,0);

  int i;
  for(i = 0; i < veces; i++){
    vector_ops_suma_par(d_A, d_B, size,
			blockx, blocky, gridx, gridy);
  }

  /* tomar el tiempo final y calcular tiempo transcurrido */ 
  cudaEventRecord(stop,0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsedTime, start, stop);

  printf("Tiempo en GPU: %g ms \n", elapsedTime);

  return 1;
}


