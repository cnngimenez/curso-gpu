#pragma once

#include <cuda.h>

int vector_io_initializeRND(float *v, int dim);
int vector_io_print(float *v, int dim);
int vector_io_initializeOnes(float *v, int dim);
