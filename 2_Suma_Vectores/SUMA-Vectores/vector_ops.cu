// #include "../../common/check.h"
#include "vector_ops.h"
#include <stdio.h>

__global__ void kernel_suma(float *v1, float *v2, int dim);

/* operaciones con vectores - implementacion */


/* Suma de vectores (inplace)  */
int vector_ops_suma_sec(float *v1, float *v2, int dim)
{

  /* suma secuencial suma inplace (el resultado queda en el paramatro v1*/
  for (int i = 0; i < dim; i++){
    v1[i] = v1[i] + v2[i];
  }

  return 1;
}





/* Suma de vectores. Resultado queda en el primer argumento */
int vector_ops_suma_par(float *v1, float *v2, int dim,
			int blockx, int blocky, int gridx, int gridy)
{

  /* configuración de la grilla. Complete con valores validos para generar 
    una grilla 1D asumiendo que dim < cantidad de threads máximo en dimension
    x de la grilla */
  dim3 nThreads(blockx, blocky);
  dim3 nBlocks(gridx, gridy);
  // dim3 nThreads(256);
  // dim3 nBlocks( (dim + nThreads.x - 1) / nThreads.x );

  /* invocacion del  kernel */
  kernel_suma<<<nBlocks, nThreads>>>(v1, v2, dim);

  cudaDeviceSynchronize();   

  cudaError_t err = cudaGetLastError();
  
  if (err != cudaSuccess){
    printf(cudaGetErrorName(err));
    printf("\n");
    printf(cudaGetErrorString(err));
    printf("\n");
    exit(1);
  }

  // check_error("kernel_suma kernel");
  
  return 1;
}




/* suma de cada elemento del vector */
__global__ void kernel_suma(float *v1, float *v2, int dim)
{

  /* setear id con el identificador del tread dentro del bloque y relativo a
    toda la grilla */
  int id = threadIdx.x + (blockIdx.x * blockDim.x);
  
  if (id < dim){
    // resolver la suma del elemento i del vector V1
    // (sumando V1 = V1 + V2 -> inplace)

    v1[id] = v1[id] + v2[id];
  }
}




/* retorna 1 si los vectores son iguales, 0 cc */
int vector_ops_iguales(float *v1, float *v2, int dim)
{
  int i;
  for(i=0; i < dim; i++) {
    if(v1[i] != v2[i]) 
      return 0;
        
  }

  return 1;
}
