#pragma once

/***********************************************/
/* Librería que implementa operaciones entre   */
/* vectores                                    */
/***********************************************/

      
int vector_ops_suma_sec(float *v1, float *v2, int dim);
int vector_ops_suma_par(float *v1, float *v2, int dim,
			int blockx, int blocky, int gridx, int gridy);
int vector_ops_iguales(float *v1, float *v2, int dim);

