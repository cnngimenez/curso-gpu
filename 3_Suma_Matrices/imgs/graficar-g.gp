reset

set title "Suma de matrices: GPU vs CPU"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
set logscale y
set style fill solid 0.5
set style fill transparent
set xrange [0:12.5]

$labels <<EOD
x y text
4 0.025 "Datos en aumento"
7.5 0.05 "Grilla lineal\nvs\nmatriz"
10.5 0.025 "Bloques desalineados"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 6.5, graph 1
set obj rect from 8.5, graph 0 to 12.5, graph 1

plot \
"results-g-2.csv" using "num":"gpu" with lines, \
"results-g-2.csv" using "num":"gpu" notitle with points, \
"results-g-2.csv" using "num":"cpu" with lines, \
"results-g-2.csv" using "num":"cpu" notitle with points, \
"results-g-2.csv" using "num":"total_gpu" title "GPU + memcpy" with lines, \
"results-g-2.csv" using "num":"total_gpu" notitle with points, \
$labels using "x":"y":"text" notitle with labels;
pause 100
