#! /usr/bin/fish

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    echo -n " "
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G"
set_color normal

echo "num gridx gridy blockx blocky tamx tamy cpu gpu HtoD DtoH memcpy" > results-g.csv
for fout in ../results/*/suma_matrices-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    set -l fdirname (dirname $fout)
    parse-times "$fout" "$fdirname/$fbasename".err >> results-g.csv
end

set_color --bold 
echo "Group T"
set_color normal

echo "num gridx gridy blockx blocky tamx tamy cpu gpu HtoD DtoH memcpy" > results-t.csv
for fout in ../results/*/suma_matrices-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    set -l fdirname (dirname "$fout")
    parse-times "$fout" "$fdirname/$fbasename".err >> results-t.csv
end
