BEGIN {
    ORS=""
}

/Dimension de la grilla: .*/ {
    match ($0, "([[:digit:]]+) x ([[:digit:]]+)", a)
    salida[0] = a[1] " " a[2]
}

/Dimension del bloque: .*/ {
    match ($0, "([[:digit:]]+) x ([[:digit:]]+)", a)
    salida[1] =  a[1] " " a[2]
}

/Tamanio de Matrix .*/ {
    match ($0, "([[:digit:]]+) x  ([[:digit:]]+)", a)
    salida[2] =  a[1] " " a[2]
}

/Suma de matrices en host .*/ {
    match ($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]]+)", a)    
    salida[3] = a[0]
}

/Suma de matrices en GPU .*/ {
    match ($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]]+)", a)
    salida[4] = a[0]
}

ENDFILE {
    match(ARGV[ARGIND], "[[:digit:]]+", a)
    print a[0] " "
    print salida[0] " " salida[1] " "
    print salida[2] " " salida[3] " "
    print salida[4]
}
