BEGIN {
    ORS=""
}

/.* \[CUDA memcpy HtoD\]/ {
    match($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]]+)(us|ms)", a)
    if (a[2] == "us"){
	a[1] = a[1] * 0.001
    }
    salida[0] = a[1]
}

/.* \[CUDA memcpy DtoH\]/ {
    match($0, "([[:digit:]]+.[[:digit:]]+|[[:digit:]])(us|ms)", a)
    if (a[2] == "us"){
	a[1] = a[1] * 0.001
    }
    salida[1] = a[1]
}

ENDFILE {
    # match(ARGV[ARGIND], "[[:digit:]]+", a)
    # print a[0] " "
    print salida[0] " " salida[1] " " salida[0] + salida[1]
}
