#! /bin/sh

# Enqueue for all groups
alias mrun='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-26] --gres=gpu:1'
# Enqueue for Group G
alias mrun_g='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-28] --gres=gpu:1'
# Enqueue for Group T
alias mrun_t='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[01-26] --gres=gpu:1'

# Parameters:
#
#     runall command parameters number-test
#
function runall {
    mrun_g nvprof --metrics all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-metrics-$3.err" &
    mrun_g nvprof --events all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-events-$3.err" &
    mrun_g nvprof ./$1 $2 > "results/$1-G-summary-$3.out" 2> "results/$1-G-summary-$3.err" &
    
    
    mrun_t nvprof --metrics all ./$1 $2 > "results/$1-T-metrics-$3.out" 2> "results/$1-T-metrics-$3.err" &
    mrun_t nvprof --events all ./$1 $2 > "results/$1-T-events-$3.out" 2> "results/$1-T-events-$3.err" &
    mrun_t nvprof ./$1 $2 > "results/$1-T-summary-$3.out" 2> "results/$1-T-summary-$3.err" &
    
}

# Sinopsis del comando: suma_matrices filas cols gridx gridy blockx blocky

# 10 blocks needed
runall "suma_matrices" "100 100 4 4 32 32" 1
# 245 blocks needed
runall "suma_matrices" "500 500 16 16 32 32" 2
# 977 blocks needed
runall "suma_matrices" "1000 1000 32 32 32 32" 3
# 6104 blocks needed
runall "suma_matrices" "2500 2500 79 79 32 32" 4

# 5000 . 5000 = 25000000
# test for aligned warps
# 1024 . 1 . 157 . 157 = 25240576 (240576 threads idle)
runall "suma_matrices" "5000 5000 157 5000 1024 1" 5
# 32 . 32 . 157 . 157 = 25240276
runall "suma_matrices" "5000 5000 157 157 32 32" 6

# test for non-aligned warps
# 1000 . 1 . 500 . 50 = 25000000
runall "suma_matrices" "5000 5000 500 5000 1000 1" 7
# 16 . 16 . 314 . 314 = 25240576
runall "suma_matrices" "5000 5000 314 314 16 16" 8
# 5 . 50 . 1000 . 1000 = 25 000 000
runall "suma_matrices" "5000 5000 1000 1000 5 50" 9
# 5 . 5 . 10000 . 10000 = 25 000 000
runall "suma_matrices" "5000 5000 10000 10000 5 5" 10

# 32 . 32 . 313 . 313 = 100320256 threads
# 10000 . 10000       = 100000000 datos
runall "suma_matrices" "10000 10000 313 313 32 32" 11
