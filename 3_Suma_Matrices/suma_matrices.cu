#include <cuda_runtime.h>
#include <stdio.h>
#include "gpu_timer.h"
#include "cpu_timer.h"

/*
 Suma de matrices
*/

void inicilizar_Matriz(float *data, const int size)
{

  int i;

  for(i = 0; i < size; i++)
    {
      data[i] = (float)(rand() / 10.0f);
    }

  return;
}

void sum_matrix_sec(float *A, float *B, float *C,
		    const int filas, const int cols)
{
  /* resolver la suma de matrices secuencial */ 
  for (int i = 0; i < filas; i++){
    for (int j = 0; j < cols; j++){
      int idx = j + (i * cols);
      C[idx] = A[idx] + B[idx];
    }
  }
  


}


void checkResult(float *hostRef, float *gpuRef, const int N)
{
  double epsilon = 1.0E-8;
  bool match = 1;

  for (int i = 0; i < N; i++)
    {
      if (abs(hostRef[i] - gpuRef[i]) > epsilon)
        {
	  match = 0;
	  printf("host %f gpu %f %d \n", hostRef[i], gpuRef[i], i);
	  break;
        }
    }

  if (match)
    printf("Test pasado! \n\n");
  else
    printf("Test no pasado! \n\n");
}



// grid 2D block 2D
__global__ void sum_matrix_par(float *MatA, float *MatB, float *MatC,
			       int cols, int filas)
{
  /* indexado usando f y c */
  int c = threadIdx.x + blockIdx.x * blockDim.x;
  int f = threadIdx.y + blockIdx.y * blockDim.y;
  /* asignar en idx el indice del thread */

  /* resolver la suma de matrices */
  // if (c < cols && f < filas)  // por qué es necesario este control?
  // Respuesta: Ver en el informe.
   
  if (c < cols && f < filas){
    int i = c + (f * cols);
    MatC[i] = MatA[i] + MatB[i];
  }
}






int main(int argc, char **argv)
{
  if (argc < 4){
    printf("Sinopsis: suma_matrices filas cols gridx gridy blockx blocky");
    exit(1);
  }

  const int filas  = atoi(argv[1]);
  const int cols   = atoi(argv[2]);
  const int gridx  = atoi(argv[3]);
  const int gridy  = atoi(argv[4]);
  const int blockx = atoi(argv[5]);
  const int blocky = atoi(argv[6]);

  printf("Dimension de la grilla: %d x %d bloques\n", gridx, gridy);
  printf("Dimension del bloque: %d x %d threads\n", blockx, blocky);
  
  
  // datos de la placa
  int dev = 0;
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, dev);
  printf("Se usa placa %d: %s\n", dev, deviceProp.name);
  cudaSetDevice(dev);


  int nBytes = filas * cols * sizeof(float);
  printf("Tamanio de Matrix : %d x  %d \n", filas, cols);
  printf("Tamanio en bytes: %d\n" ,nBytes);


  /* reservar memoria en host para las matrices h_A y h_B y las matrices 
    resultados hostRef y gpuRef */
  // malloc host memory
  float *h_A, *h_B, *hostRef, *gpuRef;
  h_A = (float *) malloc(nBytes);
  if (!h_A){
    printf("Error alocando h_A\n");
  }
  h_B = (float *) malloc(nBytes);
  if (!h_B){
    printf("Error alocando h_B\n");
  }
  hostRef = (float *) malloc(nBytes);
  if (!hostRef){
    printf("Error alocando hostRef\n");
  }
  gpuRef = (float *) malloc(nBytes);
  if (!gpuRef){
    printf("Error alocando gpuRef\n");
  }


  // inicilizacion de matrice en host
  cpu_timer crono_cpu;
  crono_cpu.tic();
  inicilizar_Matriz(h_A, filas*cols);
  inicilizar_Matriz(h_B, filas*cols);
  crono_cpu.tac();
  printf("Inicializacion de matrices en host  %f msec\n", crono_cpu.elapsed());

  /* Inicializacion de las matrices resultado en 0*/
  memset(hostRef, 0, nBytes);
  memset(gpuRef, 0, nBytes);


  // add matrix at host side for result checks
  crono_cpu.tic();
  sum_matrix_sec(h_A, h_B, hostRef, filas, cols);
  crono_cpu.tac();

  printf("Suma de matrices en host  %lf msec\n", crono_cpu.elapsed());


  /* reservar memoria en device para las matrices d_MatA, d_MatB y d_MatbC */
  // malloc device global memory
  float *d_MatA, *d_MatB, *d_MatC;
  if (cudaMalloc((void **) &d_MatA, nBytes) != cudaSuccess){
    printf("Error alocando d_MatA\n");
  }
  if (cudaMalloc((void **) &d_MatB, nBytes) != cudaSuccess){
    printf("Error alocando d_MatB\n");
  }
  if (cudaMalloc((void **) &d_MatC, nBytes) != cudaSuccess){
    printf("Error alocando d_MatC\n");
  }

    
  /* transferencia de memoria de las matrices inicializadas
    desde host a device  (h_A -> d_MatA y h_B -> d_MatB) */
  // transfer data from host to device
  if (cudaMemcpy(d_MatA, h_A, nBytes, cudaMemcpyHostToDevice) != cudaSuccess){
    printf("Error al copiar h_A -> d_MatA");
    exit(1);
  }
  if (cudaMemcpy(d_MatB, h_B, nBytes, cudaMemcpyHostToDevice) != cudaSuccess){
    printf("Error al copiar h_B -> d_MatB");
    exit(1);
  }

  /* armar una grilla de bloques de 32x32 threads, 
    con los bloques que hagan falta para solucionar el problema */
  if (blockx*blocky > deviceProp.maxThreadsPerBlock){
    printf("\nError: La cantidad de threads es mayora la que soporta la placa.\n");
    printf("%d x %d > %d\n", blockx, blocky, deviceProp.maxThreadsPerBlock );
    exit(1);
  }
    
  printf("Último índice: x = 0..%d ; y = 0..%d\n",
	 blockx-1 + ((gridx-1) * blockx),
	 blocky-1 + ((gridy-1) * blocky)
	 );
    
  // sacar los 1s y completar como corresponda
  dim3 grid (gridx, gridy);
  dim3 block(blockx, blocky);

  /* Lanzamiento del kernel al kernel */
  gpu_timer crono_gpu;
  crono_gpu.tic();

  sum_matrix_par<<<grid, block>>>(d_MatA, d_MatB, d_MatC, filas, cols);
  cudaDeviceSynchronize();

  crono_gpu.tac();
  printf("Suma de matrices en GPU  %lf msec\n", crono_gpu.ms_elapsed);


  cudaGetLastError();

  // copy kernel result back to host side
  if (cudaMemcpy(gpuRef, d_MatC, nBytes, cudaMemcpyDeviceToHost)
      != cudaSuccess){
    printf("Error al traer resultados\n");
    exit(1);
  }

  // check device results
  checkResult(hostRef, gpuRef, filas*cols);


  
 // Debug: Imprimir las matrices de salida en stdout.
  /*
   for (int f=0; f < filas; f++){
   for (int c=0; c < cols; c++){

   int idx = c + (f * cols);
   printf("%i,%i(%i): %f | %f\n",
   f, c, idx,
   hostRef[idx], gpuRef[idx]);
   }
   }
  */  
    
  /* liberar memoria de host */
  free(h_A);
  free(h_B);
  free(hostRef);
  free(gpuRef);

  /* liberar memoria de device */
  cudaFree(d_MatA);
  cudaFree(d_MatB);
  cudaFree(d_MatC);
    
  return (0);
}
