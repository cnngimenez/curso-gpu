#include <stdlib.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <time.h>
#include <cuda_runtime_api.h>

#include "../common/gpu_timer.h"
#include "../common/cpu_timer.h"

#include "../common/check.h"


/* Tamanio del array de input */
// const int N = 512*2500; // 1280000


/* Tamanio del filtro (debe ser divisor de N) */
// const int M = 32*4; // 128 

// #define SIZE_INPUT sizeof(FLOAT) * (N+m)
// #define SIZE_FILTER sizeof(FLOAT) * m
// #define SIZE_OUTPUT sizeof(FLOAT) * N

/* Funcion para preparar el filtro */
void SetupFilter(FLOAT* filter, size_t size) 
{
  for(int i = 0; i < size; i++) {
    // filter[i] = (FLOAT) (rand() % 100);
    filter[i] = 1;
  }
}


/* Convolucion en la cpu */
void conv_cpu(const FLOAT* input, FLOAT* output, const FLOAT* filter,
	      const int n, const int m) 
{
  /*
   Ayuda: se implementa la convolucion secuencial. 
   Tenga en cuenta que esta es una posible solución muy simple al 
   problema.
  */
  FLOAT temp;
	
  /*
   Barrido del vector input (tamaño N) y para cada elemento j hasta N 
   hago la operacion de convolucion: elemento i del vector filter por
   elemento i+j del vector input.
  */
  for(int j = 0; j < n; j++){	
    temp = 0.0;
    for(int i = 0; i < m; i++){
      temp += filter[i]*input[i+j];
    }
    output[j] = temp;
  }

}


/** 
  convolucion usando indexado unidimensional de threads/blocks
  un thread por cada elemento del output todo en memoria global
*/
__global__ void conv_gpu (const FLOAT* input, FLOAT* output,
			  const FLOAT* filter,
			  const int n, const int m) 
{	  	
	
  int j = (blockIdx.x * blockDim.x) + threadIdx.x ;
  	
  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  output[j] = 0.0;
  for(int i = 0; i < m; i++){
    output[j] += filter[i] * input[i+j];
  }
}



int main(int argc, char **argv) 
{
  if (argc < 2){
    printf("Synopsis: ./convolucion_float array_size filter_size");
    exit(1);
  }

  const int n = atoi(argv[1]);
  const int m = atoi(argv[2]);
  const int size_input = sizeof(FLOAT) * (n + m);
  const int size_filter = sizeof(FLOAT) * m;
  const int size_output = sizeof(FLOAT) * n;
  
  
  // --- Imprime input/output general
  printf("Input size N: %d\n", n);
  printf("Filter size M: %d\n", m);

  // --- Imprimir detalles de la placa
  int card;
  cudaGetDevice(&card);
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, card);
  printf("\nDevice %d: \"%s\" \n", card, deviceProp.name);


  // chequeo que las dimensiones N y M sean correctas para esta solucion
  assert((n % m == 0) && (m < 1024));

  // --- Reservar memoria en host
  FLOAT *h_input, *h_output, *check_output, *h_filter;
  h_input = (FLOAT *) malloc(size_input);
  h_output = (FLOAT *) malloc(size_output);
  check_output = (FLOAT *) malloc(size_output);
  h_filter = (FLOAT *) malloc(size_filter);

  assert(h_filter);
  assert(h_input);
  assert(h_output);
  assert(check_output);

  
  // --- Inicializar el filtro 
  SetupFilter(h_filter, m);

  /*
  printf("\nFilter: \n");
  for (int i=0; i < m; i++){
    printf("%.2f ", h_filter[i]);
  }
  printf("\n");
  */

  // --- Llenar input con números aleatorios
  for(int i = 0 ; i < n+m ; i++){
    h_input[i] = (FLOAT)(rand() % 100); 
  }


  // --- Reservar memoria en device  
  FLOAT *d_input, *d_output, *d_filter;
  cudaMalloc((void **) &d_input, size_input);
  cudaMalloc((void **) &d_output, size_output);
  cudaMalloc((void **) &d_filter, size_filter);

  check_errors("Mallocs");
  
  // setear a cero el device output
  cudaMemset(d_output, 0, size_output);

  check_errors("doutput <- 0");


  // --- Copiar datos al device
  gpu_timer crono_htod;
  crono_htod.tic();
  
  cudaMemcpy(d_input, h_input,
	     size_input, cudaMemcpyHostToDevice);
  // check_errors("dinput memcpy");
  cudaMemcpy(d_filter, h_filter,
	     size_filter, cudaMemcpyHostToDevice);
  // check_errors("dfilter memcpy");

  crono_htod.tac();
  check_errors("Host to Device memcpy");

  printf("Tiempo de memcpy HtoD: %lf\n", crono_htod.ms_elapsed);
  
  // --- Check en la CPU
  cpu_timer crono_cpu;
  crono_cpu.tic();
  
  conv_cpu(h_input, check_output, h_filter,
	   n, m);

  crono_cpu.tac();
  

  // --- Lanzamiento del kernel
  dim3 block_size(m);
  dim3 grid_size(n/m);

  gpu_timer crono_gpu;
  crono_gpu.tic();
  
  conv_gpu<<<grid_size, block_size>>>(d_input, d_output, d_filter, n, m);

  check_errors("conv_gpu kernel");
  crono_gpu.tac();
  

  // --- Copiar resultados
  gpu_timer crono_dtoh;
  crono_dtoh.tic();
  
  cudaMemcpy(h_output, d_output,
	     size_output, cudaMemcpyDeviceToHost);

  crono_dtoh.tac();
  check_errors("houtput <- doutput memcpy");
  printf("Tiempo de memcpy DtoH: %lf\n", crono_dtoh.ms_elapsed);


  // Usar: less -S output.out
  /*
  printf("\nFilter: \n");
  for (int i=0; i < m; i++){
    printf("%.2f ", h_filter[i]);
  }
  printf("\nInput: \n");
  for (int i=0; i < n; i++){
    printf("%.2f ", h_input[i]);
  }
  printf("\nOutput: \n");
  for (int i=0; i < n; i++){
    printf("%.2f ", h_output[i]);
  }
  printf("\nCheck Output: \n");
  for (int i=0; i < n; i++){
    printf("%.2f ", check_output[i]);
  }
  printf("\n");
  */
  
  // --- Comparación
  for(int j=0; j<n; j++){
    assert(h_output[j] == check_output[j]);
  }

  
  // --- Impresión de tiempos
  printf("[N M / ms_cpu ms_gpu]= [%d %d / %lf %lf] \n", n, m,
	 crono_cpu.ms_elapsed, crono_gpu.ms_elapsed);

  
  // --- Liberar memoria
  free(h_input);
  free(h_output);
  free(check_output);
  free(h_filter);

  cudaFree(d_input);
  cudaFree(d_output);
  cudaFree(d_filter);
  check_errors("cudafree");
}

