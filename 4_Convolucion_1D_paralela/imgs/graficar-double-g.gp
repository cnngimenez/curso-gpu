reset

set title "Convolucion 1D: Double CPU vs GPU"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
set logscale y
set style fill solid 0.5
set style fill transparent
set xrange [0:14]

plot \
"results-double-g-2.csv" using "vs_float":"gpu" with lines, \
"results-double-g-2.csv" using "vs_float":"gpu" notitle with points, \
"results-double-g-2.csv" using "vs_float":"cpu" with lines, \
"results-double-g-2.csv" using "vs_float":"cpu" notitle with points, \
"results-double-g-2.csv" using "vs_float":"total_gpu" title "GPU + memcpy" with lines, \
"results-double-g-2.csv" using "vs_float":"total_gpu" notitle with points
pause 100
