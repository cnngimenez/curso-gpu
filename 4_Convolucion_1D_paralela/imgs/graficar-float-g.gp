reset

set title "Convolucion 1D: Float CPU vs GPU"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
set logscale y
set style fill solid 0.5
set style fill transparent
set xrange [0:14]

plot \
"results-float-g-2.csv" using "num":"gpu" with lines, \
"results-float-g-2.csv" using "num":"gpu" notitle with points, \
"results-float-g-2.csv" using "num":"cpu" with lines, \
"results-float-g-2.csv" using "num":"cpu" notitle with points, \
"results-float-g-2.csv" using "num":"total_gpu" title "GPU + memcpy" with lines, \
"results-float-g-2.csv" using "num":"total_gpu" notitle with points
pause 100
