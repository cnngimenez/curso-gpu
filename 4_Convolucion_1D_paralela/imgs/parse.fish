#! /usr/bin/fish

set -l header "num tam_data tam_filter cpu gpu HtoD DtoH memcpy"

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    echo -n " "
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G - float"
set_color normal

echo "$header" > results-float-g.csv
for fout in ../results/convolucion_float-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-float-g.csv
end

set_color --bold 
echo "Group T - float"
set_color normal

echo "$header" > results-float-t.csv
for fout in ../results/convolucion_float-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-float-t.csv
end

set_color --bold 
echo "Group G - double"
set_color normal

echo "$header" > results-double-g.csv
for fout in ../results/convolucion_double-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-double-g.csv
end

set_color --bold 
echo "Group T - double"
set_color normal

echo "$header" > results-double-t.csv
for fout in ../results/convolucion_double-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-double-t.csv
end
