#include <stdlib.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <time.h>
#include <cuda_runtime_api.h>

#include "../common/gpu_timer.h"
#include "../common/cpu_timer.h"

#include "../common/check.h"

#define MAX_FILTER 256

/* Tamanio del array de input */
// const int N = 512*2500; // 1280000


/* Tamanio del filtro (debe ser divisor de N) */
// const int M = 32*4; // 128 

// #define SIZE_INPUT sizeof(FLOAT) * (N+m)
// #define SIZE_FILTER sizeof(FLOAT) * m
// #define SIZE_OUTPUT sizeof(FLOAT) * N

/* Funcion para preparar el filtro */
void SetupFilter(FLOAT* filter, size_t size) 
{
  for(int i = 0; i < size; i++) {
    // filter[i] = (FLOAT) (rand() % 100);
    filter[i] = 1;
  }
}


/* Convolucion en la cpu */
void conv_cpu(const FLOAT* input, FLOAT* output, const FLOAT* filter,
	      const int n, const int m) 
{
  /*
   Ayuda: se implementa la convolucion secuencial. 
   Tenga en cuenta que esta es una posible solución muy simple al 
   problema.
  */
  FLOAT temp;
	
  /*
   Barrido del vector input (tamaño N) y para cada elemento j hasta N 
   hago la operacion de convolucion: elemento i del vector filter por
   elemento i+j del vector input.
  */
  for(int j = 0; j < n; j++){	
    temp = 0.0;
    for(int i = 0; i < m; i++){
      temp += filter[i]*input[i+j];
    }
    output[j] = temp;
  }

}


/** 
  convolucion usando indexado unidimensional de threads/blocks
  un thread por cada elemento del output todo en memoria global
*/
__global__ void conv_gpu (const FLOAT* input, FLOAT* output,
			  const FLOAT* filter,
			  const int n, const int m) 
{	  	
	
  int j = (blockIdx.x * blockDim.x) + threadIdx.x ;
  	
  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  output[j] = 0.0;
  for(int i = 0; i < m; i++){
    output[j] += filter[i] * input[i+j];
  }
}

// declaracion del filtro en memoria constante
__constant__ FLOAT d_filtro_constant[MAX_FILTER];

/**
 convolucion utilizando el filtro en memoria constante
*/
__global__ void conv_gpu_constant_memory (const FLOAT* input, FLOAT* output,
					  const int n, const int m) 
{	  	
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  output[j] = 0.0;
  for (int i = 0; i < m; i++){
    output[j] += d_filtro_constant[i] * input[i+j];
  }
}


/* Solucion que solo sirve para Nh menor a tamaño de bloque */
__global__ void conv_gpu_shared_memory(const FLOAT *input, FLOAT *output,
				       const FLOAT *filter,
				       const int n, const int m)
{

  int tidx = blockIdx.x * blockDim.x + threadIdx.x; // global
  int id = threadIdx.x;

  __shared__ float filter_sm[MAX_FILTER];
	
  // lleno el vector de memoria compartida con los datos del filtro
  // Cada thread copia un float del filtro.
  if (id < m){
    filter_sm[id] = filter[id];
  }

  // todos los threads del bloque se deben sincronizar antes de seguir	
  __syncthreads();


  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  output[tidx] = 0.0; 
  for(int i = 0; i < m; i++){
    output[tidx] += filter_sm[i] * input[i+tidx];
  }

}



int main(int argc, char **argv) 
{
  if (argc < 2){
    printf("Synopsis: ./convolucion_float array_size filter_size\n\n");
    printf("filter_size <= 256\n");
    exit(1);
  }

  const int n = atoi(argv[1]);
  const int m = atoi(argv[2]);
  assert(m <= 256);
  const int size_input = sizeof(FLOAT) * (n + m);
  const int size_filter = sizeof(FLOAT) * m;
  const int size_output = sizeof(FLOAT) * n;
  
  
  // --- Imprime input/output general
  printf("Input size N: %d\n", n);
  printf("Filter size M: %d\n", m);

  // --- Imprimir detalles de la placa
  int card;
  cudaGetDevice(&card);
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, card);
  printf("\nDevice %d: \"%s\" \n", card, deviceProp.name);


  // chequeo que las dimensiones N y M sean correctas para esta solucion
  assert((n % m == 0) && (m < 1024));

  // --- Reservar memoria en host
  FLOAT *h_input, *h_output, *check_output, *h_filter;
  h_input = (FLOAT *) malloc(size_input);
  h_output = (FLOAT *) malloc(size_output);
  check_output = (FLOAT *) malloc(size_output);
  h_filter = (FLOAT *) malloc(size_filter);

  assert(h_filter);
  assert(h_input);
  assert(h_output);
  assert(check_output);

  
  // --- Inicializar el filtro 
  SetupFilter(h_filter, m);

  /*
  printf("\nFilter: \n");
  for (int i=0; i < m; i++){
    printf("%.2f ", h_filter[i]);
  }
  printf("\n");
  */

  // --- Llenar input con números aleatorios
  for(int i = 0 ; i < n+m ; i++){
    h_input[i] = (FLOAT)(rand() % 100); 
  }


  // --- Reservar memoria en device  
  FLOAT *d_input, *d_output, *d_filter, *d_output_sm, *d_output_cm;
  cudaMalloc((void **) &d_input, size_input);
  cudaMalloc((void **) &d_output, size_output);
  cudaMalloc((void **) &d_output_sm, size_output);
  cudaMalloc((void **) &d_output_cm, size_output);
  cudaMalloc((void **) &d_filter, size_filter);
  

  check_errors("Mallocs");
  
  // setear a cero el device output
  cudaMemset(d_output, 0, size_output);
  cudaMemset(d_output_sm,0, n * sizeof(FLOAT));
  cudaMemset(d_output_cm,0, n * sizeof(FLOAT));

  check_errors("doutput <- 0");


  // --- Copiar datos al device
  gpu_timer crono_htod;
  crono_htod.tic();  
  cudaMemcpy(d_input, h_input,
	     size_input, cudaMemcpyHostToDevice);
  cudaMemcpy(d_filter, h_filter,
	     size_filter, cudaMemcpyHostToDevice);
  crono_htod.tac();
  check_errors("Host to Device memcpy");

  printf("d_filter y d_input, tiempo de memcpy HtoD: %lf\n",
	 crono_htod.ms_elapsed);
  
  // --- Check en la CPU
  cpu_timer crono_cpu;
  crono_cpu.tic();  
  conv_cpu(h_input, check_output, h_filter,
	   n, m);
  crono_cpu.tac();
  

  // --- Lanzamiento del kernel
  dim3 block_size(m);
  dim3 grid_size(n/m);

  gpu_timer crono_gpu;
  crono_gpu.tic();
  
  conv_gpu<<<grid_size, block_size>>>(d_input, d_output, d_filter, n, m);

  crono_gpu.tac();
  check_errors("conv_gpu kernel");  

  // --- Copiar resultados
  gpu_timer crono_dtoh;
  crono_dtoh.tic();   
  cudaMemcpy(h_output, d_output,
	     size_output, cudaMemcpyDeviceToHost);
  crono_dtoh.tac();
  check_errors("houtput <- doutput memcpy");
  printf("houtput <- doutput tiempo de memcpy DtoH: %lf\n",
	 crono_dtoh.ms_elapsed);


  // --- Comparación
  for(int j=0; j<n; j++){
    assert(h_output[j] == check_output[j]);
  }


  /*****************************************************/
  /* VERSION PARALELA  -  FILTRO EN MEMORIA COMPARTIDA */
  /*****************************************************/

  gpu_timer crono_gpu_sm;
  crono_gpu_sm.tic();   
  // lanzamiento del kernel que usa memoria compartida para filtro.
  // Salida queda en d_output_sm.
  conv_gpu_shared_memory<<<grid_size, block_size>>>(
						d_input, d_output_sm,
						d_filter,
						n, m);
  crono_gpu_sm.tac();
  check_errors("conv_gpu_shared_memory");

  gpu_timer crono_dtoh_sm;
  crono_dtoh_sm.tic();
  cudaMemcpy(h_output, d_output_sm,
	     size_output, cudaMemcpyDeviceToHost);
  crono_dtoh_sm.tac();
  check_errors("h_output <- d_output_sm memcpy");
  

  // Comparacion
  for(int j=0; j<n; j++){
    assert(h_output[j] == check_output[j]);
  }


  /****************************************************/
  /* VERSION PARALELA  -  MEMORIA DE CONSTANTES   */
  /****************************************************/

  cudaMemcpyToSymbol(d_filtro_constant, h_filter, size_filter);

  check_errors("d_filtro_constant <- h_filter memcpy");
  
  gpu_timer crono_gpu_cm;
  crono_gpu_cm.tic();
   
  // lanzamiento del kernel que usa memoria de constantes para filtro.
  // Salida queda en d_output_cm.
  conv_gpu_constant_memory<<<grid_size, block_size>>>(
						  d_input, d_output_cm,
						  n, m);
 
  crono_gpu_cm.tac();
  check_errors("kernel conv_gpu_constant_memory");
  
  cudaMemcpy(h_output, d_output_cm,
	     size_output, cudaMemcpyDeviceToHost);
  check_errors("h_output <- d_output_cm memcpy");

  // Comparacion 
  for(int j=0; j<n; j++){
    assert(h_output[j] == check_output[j]);
  }

  
  /* Impresion de tiempos */
  printf("[N/M/ms_cpu/ms_gpu/ms_gpu_sm/ms_gpu_cm]= [%d/%d/%lf/%lf/%lf/%lf] \n",
	 n, m,
	 crono_cpu.ms_elapsed,
	 crono_gpu.ms_elapsed,
	 crono_gpu_sm.ms_elapsed,
	 crono_gpu_cm.ms_elapsed);
 
  
  // --- Liberar memoria
  free(h_input);
  free(h_output);
  free(check_output);
  free(h_filter);

  cudaFree(d_input);
  cudaFree(d_output);
  cudaFree(d_output_cm);
  cudaFree(d_output_sm);
  cudaFree(d_filter);
  check_errors("cudafree");
}

