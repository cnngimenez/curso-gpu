#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <time.h>
#include <cuda_runtime_api.h>

#include "gpu_timer.h"
#include "cpu_timer.h"



/* Tamanio del array de input */
const int N = 512*3500; 

/* Tamanio del filtro */
const int M = 32*32;

/* Floating point type */
typedef float FLOAT;
//typedef double FLOAT;

#define SIZE_INPUT sizeof(FLOAT) * (N+M)
#define SIZE_FILTER sizeof(FLOAT) * M
#define SIZE_OUTPUT sizeof(FLOAT) * N

/**
 Return if there's an error.

 Exit program if error is not cudaSuccess.
 */
void check_errors(const char* mesg){
  cudaError_t err = cudaGetLastError();
  
  if (err != cudaSuccess){
    printf(mesg);
    printf(cudaGetErrorName(err));
    printf("\n");
    printf(cudaGetErrorString(err));
    printf("\n");
    exit(1);
  }
    
}

/* Funcion para preparar el filtro */
void SetupFilter(FLOAT* filter, size_t size) 
{
  for(int i = 0; i < size; i++){
    filter[i] = (FLOAT) (rand() % 100);
    // filter[i] = 1;
  }
}


/* Convolucion en la cpu */
void conv_cpu(const FLOAT* input, FLOAT* output, const FLOAT* filter) 
{
  FLOAT temp;
  /*
   Barrido vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento i+j
   del vector input
  */

  for (int j=0; j < N; j++){
    temp = 0.0;
    for (int i = 0; i < M; i++){
      temp += filter[i] * input[i+j];
    }
    output[j] = temp;
  }
}


// declaracion del filtro en memoria constante
__constant__ FLOAT d_filtro_constant[M];

/**
 convolucion utilizando el filtro en memoria constante
*/
__global__ void conv_gpu_constant_memory (const FLOAT* input, FLOAT* output) 
{	  	
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  output[j] = 0.0;
  for (int i = 0; i < M; i++){
    output[j] += d_filtro_constant[i] * input[i+j];
  }
}


// convolucion usando indexado unidimensional de threads/blocks
// un thread por cada elemento del output todo en memoria global
__global__ void conv_gpu (const FLOAT* input, FLOAT* output, const FLOAT* filter) 
{	  	
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  output[j] = 0.0;
  for(int i = 0; i < M; i++){
    output[j] += filter[i] * input[i+j];
  }
}



/* Solucion que solo sirve para Nh menor a tamaño de bloque */
__global__ void conv_gpu_shared_memory(const FLOAT *input, FLOAT *output, const FLOAT *filter)
{

  int tidx = blockIdx.x * blockDim.x + threadIdx.x; // global
  int id = threadIdx.x;

  __shared__ float filter_sm[M];
	
  // lleno el vector de memoria compartida con los datos del filtro
  // Cada thread copia un float del filtro.
  if (id < M){
    filter_sm[id] = filter[id];
  }

  // todos los threads del bloque se deben sincronizar antes de seguir	
  __syncthreads();


  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  output[tidx] = 0.0;
  for(int i = 0; i < M; i++){
    output[tidx] += filter_sm[i] * input[i+tidx];
  }

}

int main() 
{

  /* Imprime input/output general */
  printf("Input size N: %d\n", N);
  printf("Filter size M: %d\n", M);

  /* se imprime el nobre de la placa */
  int card;
  cudaGetDevice(&card);
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, card);
  printf("\nDevice %d: \"%s\" \n", card, deviceProp.name);

  /* chequeos de dimensiones de senial y filtro */
  assert(N % M == 0);
  assert(M <= 1024);


  /* TODO: aloque memoria en host para el input, output, check output y filtro  */
  FLOAT *h_input, *h_output, *check_output, *h_filter;
  h_input = (FLOAT *) malloc(SIZE_INPUT);
  h_output = (FLOAT *) malloc(SIZE_OUTPUT);
  check_output = (FLOAT *) malloc(SIZE_OUTPUT);
  h_filter = (FLOAT *) malloc(SIZE_FILTER);

  assert(h_filter);
  assert(h_input);
  assert(h_output);
  assert(check_output);

  
  /* Inicializa el filtro */
  SetupFilter(h_filter, M);

  /* Llena el array de input (CON "padding") con numeros aleatorios acotados */
  for(int i = 0 ; i < N+M ; i++){
    h_input[i] = (FLOAT)(rand() % 100); 
  }

  // alocar memoria en device para el input, filtro,
  // y los outputs de las 3 versiones en GPU
  FLOAT *d_input, *d_output, *d_filter, *d_output_sm, *d_output_cm;
  cudaMalloc((void **) &d_input, SIZE_INPUT);
  cudaMalloc((void **) &d_output, SIZE_OUTPUT);
  cudaMalloc((void **) &d_output_sm, SIZE_OUTPUT);
  cudaMalloc((void **) &d_output_cm, SIZE_OUTPUT);
  cudaMalloc((void **) &d_filter, SIZE_FILTER);

  check_errors("cudaMallocs");
    
  // pongo a cero el device output
  cudaMemset(d_output,0,N * sizeof(FLOAT));
  cudaMemset(d_output_sm,0,N * sizeof(FLOAT));
  cudaMemset(d_output_cm,0,N * sizeof(FLOAT));

  // copiar senial de entrada (h_input) y filtro en GPU
  cudaMemcpy(d_input, h_input,
	     SIZE_INPUT, cudaMemcpyHostToDevice);
  check_errors("dinput memcpy");
  cudaMemcpy(d_filter, h_filter,
	     SIZE_FILTER, cudaMemcpyHostToDevice);
  check_errors("dfilter memcpy");

  
  /* cronometraje */
  cpu_timer crono_cpu; 
  crono_cpu.tic();

  /* convolucion en la CPU */
  conv_cpu(h_input, check_output, h_filter);

  crono_cpu.tac();

  /****************************************************/
  /* VERSION PARALELA  -  FILTRO EN MEMORIA GLOBAL    */
  /****************************************************/

  /*Defino tamaño bloque y grilla */
  dim3 block_size(M);
  dim3 grid_size(N/M);

  gpu_timer crono_gpu;
  crono_gpu.tic();
    
  // lanzamiento del kernel que usa memoria global para filtro y senial.
  // Salida queda en d_output.
  conv_gpu<<<grid_size, block_size>>>(d_input, d_output, d_filter);

  crono_gpu.tac();
  check_errors("conv_gpu");
  
  cudaMemcpy(h_output, d_output,
	     SIZE_OUTPUT, cudaMemcpyDeviceToHost);
  check_errors("h_output <- d_output memcpy");
  
  // Comparacion 	
  for(int j=0; j<N; j++){
    assert(h_output[j] == check_output[j]);
  }



  /*****************************************************/
  /* VERSION PARALELA  -  FILTRO EN MEMORIA COMPARTIDA */
  /*****************************************************/

  gpu_timer crono_gpu_sm;
  crono_gpu_sm.tic();
   
  // lanzamiento del kernel que usa memoria compartida para filtro.
  // Salida queda en d_output_sm.
  conv_gpu_shared_memory<<<grid_size, block_size>>>(
						d_input, d_output_sm,
						d_filter);

  crono_gpu_sm.tac();
  check_errors("conv_gpu_shared_memory");

  cudaMemcpy(h_output, d_output_sm,
	     SIZE_OUTPUT, cudaMemcpyDeviceToHost);
  check_errors("h_output <- d_output_sm memcpy");
  

  // Comparacion
  for(int j=0; j<N; j++){
    assert(h_output[j] == check_output[j]);
  }


  /****************************************************/
  /* VERSION PARALELA  -  MEMORIA DE CONSTANTES   */
  /****************************************************/

  cudaMemcpyToSymbol(d_filtro_constant, h_filter, SIZE_FILTER);

  check_errors("d_filtro_constant <- h_filter memcpy");
  
  gpu_timer crono_gpu_cm;
  crono_gpu_cm.tic();
   
  // lanzamiento del kernel que usa memoria de constantes para filtro.
  // Salida queda en d_output_cm.
  conv_gpu_constant_memory<<<grid_size, block_size>>>(d_input, d_output_cm);
 
  crono_gpu_cm.tac();
  check_errors("kernel conv_gpu_constant_memory");
  
  cudaMemcpy(h_output, d_output_cm,
	     SIZE_OUTPUT, cudaMemcpyDeviceToHost);
  check_errors("h_output <- d_output_cm memcpy");

  // Comparacion 
  for(int j=0; j<N; j++){
    assert(h_output[j] == check_output[j]);
  }

	
	
  /* Impresion de tiempos */
  printf("[N/M/ms_cpu/ms_gpu/ms_gpu_sm/ms_gpu_cm]= [%d/%d/%lf/%lf/%lf/%lf] \n",
	 N, M,
	 crono_cpu.ms_elapsed,
	 crono_gpu.ms_elapsed, crono_gpu_sm.ms_elapsed,
	 crono_gpu_cm.ms_elapsed);


   

  /* TODO: liberer memoria en host y device */
  free(h_output);
  free(h_input);
  free(h_filter);
  free(check_output);

  cudaFree(d_output_cm);
  cudaFree(d_output);
  cudaFree(d_output_sm);
  cudaFree(d_input);
  cudaFree(d_filter);
    
  check_errors("cudaFree");
}

