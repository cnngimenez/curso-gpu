reset
set terminal png size 1360,680
set output 'plots/results-double_vs_float.png'

set title "Convolucion 1D: Float vs Double"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
# set logscale y
set style fill solid 0.5
set style fill transparent
set style data histograms
set style histogram cluster gap 1
set xrange [-1:13.5]
set yrange [0:18]

$labels <<EOD
x y text
4 12.5 "Tam. arreglo constante\nTam. filtro en aumento"
7 12.5 "Tam. arreglo en aumento\nTam. filtro constante"
# 12 10 "< Float"
# 14.5 10 "Double >"
# 17 1 "Tam. arreglo constante\nTam. filtro en aumento"
# 24 1 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from -1, graph 0 to 5.5, graph 1
# set obj rect from 13.5, graph 0 to 20.5, graph 1


plot \
     $labels using "x":"y":"text" notitle with labels, \
     "csv/float-g.csv" using "gpu":xtic(1) \
     title "GPU Float" fillstyle pattern 1 border lc "red", \
     "csv/double-g.csv" using "gpu":xtic(1) \
     title "GPU Double" fillstyle pattern 4 border lc "red", \
     "csv/float-g.csv" using "gpu_sm":xtic(1) \
     title "Float Shared Mem." fillstyle pattern 1 border lc "cyan", \
     "csv/double-g.csv" using "gpu_sm":xtic(1) \
     title "Double Shared Mem." fillstyle pattern 4 border lc "cyan", \
     "csv/float-g.csv" using "gpu_cm":xtic(1) \
     title "Float Const. Mem." fillstyle pattern 1 border lc "blue", \
     "csv/double-g.csv" using "gpu_cm":xtic(1) \
     title "Double Const. Mem." fillstyle pattern 4 border lc "blue"
quit
# pause 100
