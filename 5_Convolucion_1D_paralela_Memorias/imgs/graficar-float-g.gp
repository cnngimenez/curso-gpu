reset
set terminal png size 1360,680
set output 'plots/results-float-g.png'

set title "Convolucion 1D-Float: Memorias"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
# set logscale y
set style fill solid 0.5
set style fill transparent
set xrange [0:13]

$labels <<EOD
x y text
4 1 "Tam. arreglo constante\nTam. filtro en aumento"
11 1 "Tam. arreglo en aumento\nTam. filtro constante"
12 10 "< Float"
14 10 "Double >"
17 1 "Tam. arreglo constante\nTam. filtro en aumento"
24 1 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 7.5, graph 1
set obj rect from 13.5, graph 0 to 20.5, graph 1

plot \
     $labels using "x":"y":"text" notitle with labels, \
     "csv/results-g.csv" using "num":"gpu" title "GPU Global Mem." with lines, \
     "csv/results-g.csv" using "num":"gpu" notitle with points, \
     "csv/results-g.csv" using "num":"gpu_sm" title "GPU Shared Mem." with lines, \
     "csv/results-g.csv" using "num":"gpu_sm" notitle with points, \
     "csv/results-g.csv" using "num":"gpu_cm" title "GPU Const. Mem." with lines, \
     "csv/results-g.csv" using "num":"gpu_cm" notitle with points
quit
# pause 100
