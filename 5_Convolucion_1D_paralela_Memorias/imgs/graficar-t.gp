reset
set terminal png size 1360,680
set output 'plots/results-t.png'

set title "Suma de Vectores: GPU Tesla vs CPU"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Tiempo (ms)"
set logscale y
set style fill solid 0.5
set style fill transparent
set xrange [0:14]

plot \
"csv/results-t.csv" using "num":"gpu" with filledcurves below x1, \
"csv/results-t.csv" using "num":"gpu" notitle with points, \
"csv/results-t.csv" using "num":"cpu" with filledcurves, \
"csv/results-t.csv" using "num":"cpu" notitle with points
quit
# pause 100
