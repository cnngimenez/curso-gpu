reset
set terminal png size 1360,680
set output 'plots/l2-tex-write-hit-rate.png'

set title "Convolucion 1D: L2 Write Texture Hit Rate"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Percentage (%)"
# set logscale y
set style fill solid 0.5
set style fill transparent
# set yrange [-10:110]
set xrange [0:27]

$labels <<EOD
x y text
4 97.5 "Tam. arreglo constante\nTam. filtro en aumento"
10.5 97.5 "Tam. arreglo en aumento\nTam. filtro constante"
12.5 98 "< Float"
14.5 98 "Double >"
17 97.5 "Tam. arreglo constante\nTam. filtro en aumento"
24 97.5 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 7.5, graph 1
set obj rect from 13.5, graph 0 to 20.5, graph 1

plot \
$labels using "x":"y":"text" notitle with labels, \
"csv/l2_tex_write_hit_rate-gm.csv" using "Test Number":"Avg" title "Global Mem." with lines, \
"csv/l2_tex_write_hit_rate-gm.csv" using "Test Number":"Avg" notitle with points, \
"csv/l2_tex_write_hit_rate-cm.csv" using "Test Number":"Avg" title "Constant Mem." with lines, \
"csv/l2_tex_write_hit_rate-cm.csv" using "Test Number":"Avg" notitle with points, \
"csv/l2_tex_write_hit_rate-sm.csv" using "Test Number":"Avg" title "Shared Mem." with lines, \
"csv/l2_tex_write_hit_rate-sm.csv" using "Test Number":"Avg" notitle with points
quit
# pause 100
