reset
set terminal png size 1360,680
set output 'plots/local-hit-rate.png'

set title "Convolucion 1D: Local Hit Rate"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Rate (%)"
# set logscale y
set style fill solid 0.5
set style fill transparent
#set yrange [30:60]
set xrange [0:27]

$labels <<EOD
x y text
4 -0.5 "Tam. arreglo constante\nTam. filtro en aumento"
11 -0.5 "Tam. arreglo en aumento\nTam. filtro constante"
12.5 0.5 "< Float"
14.5 0.5 "Double >"
17 -0.5 "Tam. arreglo constante\nTam. filtro en aumento"
24 -0.5 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 7.5, graph 1
set obj rect from 13.5, graph 0 to 20.5, graph 1

plot \
$labels using "x":"y":"text" notitle with labels, \
"csv/Local Hit Rate-gm.csv" using "Test Number":"Avg" title "Global Mem." with lines, \
"csv/Local Hit Rate-gm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Local Hit Rate-cm.csv" using "Test Number":"Avg" title "Constant Mem." with lines, \
"csv/Local Hit Rate-cm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Local Hit Rate-sm.csv" using "Test Number":"Avg" title "Shared Mem." with lines, \
"csv/Local Hit Rate-sm.csv" using "Test Number":"Avg" notitle with points
quit
# pause 100
