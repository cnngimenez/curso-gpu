#! /usr/bin/awk -f
 
# Copyright 2019 Christian Gimenez

# Author: Christian Gimenez

# metrics.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BEGIN {
    RS="\n"          # Record separator    
    FS="[[:space:]][[:space:]]+"           # Field separator    
    OFS=" "          # Output field separator
    ORS="\n"         # Output record separator

    # print "\"Test Number\"", "\"Invocations\"", "\"Metric Name\"", "\"Metric Description\"", "\"Min\"", "\"Max\"", "\"Avg\"", "\"Kernel\""
    match(ARGV[1], /.*metrics-([[:digit:]]+)/, matches)
    testnum=matches[1]
}

/^[[:space:]]*Kernel:/ {
    match($0, /Kernel:[[:space:]]*([^\(]*)/, matches)
    kernel=matches[1]
}

/^[[:space:]]*[[:digit:]]/ {
    split($0, fields, /[[:space:]][[:space:]]*/)
    print testnum, $2, $3, "\"" $4 "\"", $5, $6, $7, kernel
}

END {
}
