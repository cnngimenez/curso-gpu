#! /usr/bin/fish

set_color --bold
echo "Cleaning..."
set_color normal

rm csv/*

# ############################################################################
# -> G Group

set -l header "num tam_data tam_filter cpu gpu gpu_sm gpu_cm HtoD DtoH memcpy"

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G - float"
set_color normal

echo "$header" > csv/results-g.csv
for fout in ../results/convolucion_float-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> csv/results-g.csv
end

set_color --bold 
echo "Group G - double"
set_color normal

for fout in ../results/convolucion_double-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> csv/results-g.csv
end

# No use to calculate gpu_time = gpu + memcpy because the program copies
# the following: filter in the constant memory, three outputs memory
# setted to zero, input data once. All these copies are summed up in the
# memcpy number.

# gawk -f gpu_time.awk csv/results-g.csv > csv/results-g-final.csv

# ########################################################################
# -> T Group

set_color --bold 
echo "Group T - float"
set_color normal

echo "$header" > csv/results-t.csv
for fout in ../results/convolucion_float-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> csv/results-t.csv
end

set_color --bold 
echo "Group T - double"
set_color normal

for fout in ../results/convolucion_double-T-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> csv/results-t.csv
end

# ############################################################################

set_color --bold
echo "Group G - Double vs Float"
set_color normal

# echo "$header" > csv/double_vs_float-g.csv
head -n 14 csv/results-g.csv > csv/float-g.csv

echo -n "vs_num " > csv/double-g.csv
head -n 1 csv/results-g.csv >> csv/double-g.csv
tail -n 13 csv/results-g.csv | nl -w 2 -s' ' >> csv/double-g.csv

# ############################################################################

set_color --bold
echo "Group G - Metrics"
set_color normal

# Metrics
echo "\"Test Number\" \"Invocations\" \"Metric Name\" \"Metric Description\" \"Min\" \"Max\" \"Avg\" \"Kernel\"" > csv/results-metrics.csv
for fout in ../results/convolucion_float-G-metrics-*.err
    gawk -f metrics.awk -- "$fout" >> csv/results-metrics.csv
end

for fout in ../results/convolucion_double-G-metrics-*.err
    gawk -f metrics.awk -- "$fout" >> csv/results-metrics.csv
end

# ############################################################################

set_color --bold
echo "Group G - Events"
set_color normal

echo "\"Test Number\" \"Invocations\" \"Event Name\" \"Min\" \"Max\" \"Avg\" \"Total\" \"Kernel\"" > csv/results-events.csv
for fout in ../results/convolucion_float-G-events-*.err
    gawk -f events.awk -- "$fout" >> csv/results-events.csv
end

for fout in ../results/convolucion_double-G-events-*.err
    gawk -f events.awk -- "$fout" >> csv/results-events.csv
end

# ############################################################################

function parse-metric
    echo "$argv[1]"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-gm.csv"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-cm.csv"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-sm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep 'conv_gpu$'  >> "csv/$argv[1]-gm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep "conv_gpu_constant_memory"  >> "csv/$argv[1]-cm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep "conv_gpu_shared_memory"  >> "csv/$argv[1]-sm.csv"
end

set_color --bold
echo "Group G - Selected Metrics"
set_color normal

parse-metric "Unified Cache Throughput"
parse-metric "Unified Cache Hit Rate"
parse-metric "tex_cache_transactions"
parse-metric "Global Hit Rate"
parse-metric "Local Hit Rate"
parse-metric "Shared Memory Efficiency"
parse-metric "Shared Memory Utilization"
parse-metric "stall_memory_dependency"
parse-metric "stall_constant_memory_dependency"
parse-metric "l2_tex_read_hit_rate"
parse-metric "l2_tex_write_hit_rate"
parse-metric "l2_tex_read_throughput"
parse-metric "Issue Slot Utilization"
parse-metric "gld_transactions "
parse-metric "gst_transactions "
parse-metric "l2_tex_read_transactions"
parse-metric "l2_tex_write_transactions"
parse-metric "l2_read_transactions"
parse-metric "l2_write_transactions"
