reset
set terminal png size 1360,680
set output 'plots/shared-memory-utilization.png'

set title "Convolucion 1D: Shared Memory Utilization"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Percentage (%)"
# set logscale y
set style fill solid 0.5
set style fill transparent
set yrange [-10:110]
set xrange [0:27]

$labels <<EOD
x y text
4 40 "Tam. arreglo constante\nTam. filtro en aumento"
10.5 40 "Tam. arreglo en aumento\nTam. filtro constante"
12.5 55 "< Float"
14.5 55 "Double >"
17 40 "Tam. arreglo constante\nTam. filtro en aumento"
24 40 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 7.5, graph 1
set obj rect from 13.5, graph 0 to 20.5, graph 1

plot \
$labels using "x":"y":"text" notitle with labels, \
"csv/Shared Memory Utilization-gm.csv" using "Test Number":"Avg" title "Global Mem." with lines, \
"csv/Shared Memory Utilization-gm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Shared Memory Utilization-cm.csv" using "Test Number":"Avg" title "Constant Mem." with lines, \
"csv/Shared Memory Utilization-cm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Shared Memory Utilization-sm.csv" using "Test Number":"Avg" title "Shared Mem." with lines, \
"csv/Shared Memory Utilization-sm.csv" using "Test Number":"Avg" notitle with points
quit
# pause 100
