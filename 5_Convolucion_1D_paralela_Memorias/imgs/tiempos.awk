BEGIN {
    ORS=""
}

/Input size N: .*/ {
    match ($0, "([[:digit:]]+)", a)
    salida[0] = a[1]
}

/Filter size M: .*/ {
    match ($0, "([[:digit:]]+)", a)
    salida[1] =  a[1]
}

/\[N\/M\/ms_cpu\/ms_gpu\/ms_gpu_sm\/ms_gpu_cm\]= / {
    match($0, /= \[(.*)\]/, a)
    split(a[1], b, "/")
    salida[2] = b[3]
    salida[3] = b[4]
    salida[4] = b[5]
    salida[5] = b[6]
}

ENDFILE {
    match(ARGV[ARGIND], "[[:digit:]]+", a)
    print a[0] " "
    print salida[0] " " salida[1] " "
    print salida[2] " " salida[3] " "
    print salida[4] " " salida[5] " "
    print salida[6]
}
