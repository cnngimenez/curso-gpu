reset
set terminal png size 1360,680
set output 'plots/unified-cache-throughput.png'

set title "Convolucion 1D: Unified Cache Throughput"
set xtics 0,1
set xlabel "Número de Experimento"
set ytics
set ylabel "Throughput (GB/s)"
# set logscale y
set style fill solid 0.5
set style fill transparent
set yrange [100:600]
set xrange [0:27]

$labels <<EOD
x y text
4 150 "Tam. arreglo constante\nTam. filtro en aumento"
11 150 "Tam. arreglo en aumento\nTam. filtro constante"
12.5 550 "< Float"
14.5 550 "Double >"
17 150 "Tam. arreglo constante\nTam. filtro en aumento"
24 150 "Tam. arreglo en aumento\nTam. filtro constante"
EOD
set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0.5, graph 0 to 7.5, graph 1
set obj rect from 13.5, graph 0 to 20.5, graph 1

plot \
$labels using "x":"y":"text" notitle with labels, \
"csv/Unified Cache Throughput-gm.csv" using "Test Number":"Avg" title "Global Mem." with lines, \
"csv/Unified Cache Throughput-gm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Unified Cache Throughput-cm.csv" using "Test Number":"Avg" title "Constant Mem." with lines, \
"csv/Unified Cache Throughput-cm.csv" using "Test Number":"Avg" notitle with points, \
"csv/Unified Cache Throughput-sm.csv" using "Test Number":"Avg" title "Shared Mem." with lines, \
"csv/Unified Cache Throughput-sm.csv" using "Test Number":"Avg" notitle with points
quit
# pause 100
