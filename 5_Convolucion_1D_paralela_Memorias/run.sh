#! /bin/sh

# Enqueue for all groups
alias mrun='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-26] --gres=gpu:1'
# Enqueue for Group G
alias mrun_g='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-28] --gres=gpu:1'
# Enqueue for Group T
alias mrun_t='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[01-26] --gres=gpu:1'

# Parameters:
#
#     runall command parameters number-test
#
function runall {
    mrun_g nvprof --metrics all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-metrics-$3.err" &
    mrun_g nvprof --events all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-events-$3.err" &
    mrun_g nvprof ./$1 $2 > "results/$1-G-summary-$3.out" 2> "results/$1-G-summary-$3.err" &
    
    
    mrun_t nvprof --metrics all ./$1 $2 > "results/$1-T-metrics-$3.out" 2> "results/$1-T-metrics-$3.err" &
    mrun_t nvprof --events all ./$1 $2 > "results/$1-T-events-$3.out" 2> "results/$1-T-events-$3.err" &
    mrun_t nvprof ./$1 $2 > "results/$1-T-summary-$3.out" 2> "results/$1-T-summary-$3.err" &
    
}

# Synopsis: convolucion_float array_size filter_size
# Synopsis: convolucion_double array_size filter_size

# Must comply:
# (array_size mod filter_size == 0) and (filter_size < 1024)

runall "convolucion_float"  "1280000 32"  1
runall "convolucion_float"  "1280000 64"  2
runall "convolucion_float"  "1280000 100" 3
runall "convolucion_float"  "1280000 128" 4
runall "convolucion_float"  "1280000 160" 5
runall "convolucion_float"  "1280000 200" 6
runall "convolucion_float"  "1280000 256" 7
 
runall "convolucion_float"  "320000 128"   8
runall "convolucion_float"  "640000 128"   9
runall "convolucion_float"  "1280000 128" 10
runall "convolucion_float"  "1920000 128" 11
runall "convolucion_float"  "2560000 128" 12
runall "convolucion_float"  "3200000 128" 13
 
runall "convolucion_double"  "1280000 32"  14
runall "convolucion_double"  "1280000 64"  15
runall "convolucion_double"  "1280000 100" 16
runall "convolucion_double"  "1280000 128" 17
runall "convolucion_double"  "1280000 160" 18
runall "convolucion_double"  "1280000 200" 19
runall "convolucion_double"  "1280000 256" 20
 
runall "convolucion_double"  "320000 128"  21
runall "convolucion_double"  "640000 128"  22
runall "convolucion_double"  "1280000 128" 23
runall "convolucion_double"  "1920000 128" 24
runall "convolucion_double"  "2560000 128" 25
runall "convolucion_double"  "3200000 128" 26
