#ifndef _defs_h
#define _defs_h

#define Y 750
#define X 499

#define SIZE_IMG sizeof(float) * X * Y

// Evitamos usar divisiones: la mitad del filtro
// Para un filtro de 3 x 3: floor(max_cols) = 1
#define FILTRO_RCOLS 1
#define FILTRO_RFILAS 1

#define FILTRO_COLS 3
#define FILTRO_FILAS 3
// Evitamos usar multiplicación
// TAM_FILTRO = FILTRO_COLS * FILTRO_FILAS
#define TAM_FILTRO 9
// TAM_FILTRO_1 = 1 / TAM_FILTRO
#define TAM_FILTRO_1 0.11111111

#define SIZE_FILTRO sizeof(float) * 9

#define INPUT "Antonov.txt"

#endif 
