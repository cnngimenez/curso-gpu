#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "imagen.h"

/***************************************************/
/* ENTRADA Y SALIDA DE IMAGENES                    */
/***************************************************/

/* Lee una imagen desde un archivo y la deja en memria principal */
void leer_imagen(const char * nombre, float *imagen, int filas, int cols)
{
  FILE *input = fopen(nombre,"r");

  if (input == NULL)
    {
      printf("Error en leer_imagen: no abre archivo %s \n", nombre);
      exit(-1);
    }

  int i,j, dato;


  for(i = 0; i < filas; i++)
    {
      for(j = 0; j < cols; j++)
	{
	  fscanf(input, "%d", &dato);
	  imagen[i*cols + j] = (float)dato;
	}
		
    }
  fclose(input);

}



/* Esta funcion esta hecha especialmente para ser usada por GNUPLOT que invierte las matrices */
void salvar_imagen(const char * nombre, float *imagen, int filas, int cols)
{
  FILE *output = fopen(nombre,"w");

  if (output == NULL)
    {
      printf("Error en salvar: no abre archivo %s \n", nombre);
      exit(-1);
    }

  printf("Escribiendo en archivo: %s \n", nombre);

  int i,j;
  float dato;
  for(i = 0; i < filas; i++) {
    for(j = 0; j < cols; j++) {

      if ((i == 0) || (i == filas-1) || (j == 0) || (j == cols-1)) 
	dato = 0.0;
      else
	dato = imagen[(filas-i)*cols + j];  // arranco desde abajo porque gnuplot grafica al reves
      fprintf(output,"%.0f ",dato); 
			
    }
    fprintf(output, "\n");
  }
  fclose(output);

}


/*************************************************************/
/* Inicializaciones de filtros                               */
/*************************************************************/

/* inicializacion para aplicar filtro promedio  */
void inicializar_filtro_promedio(float *filtro, int tamFiltro)
{
  int i;

  for (i=0; i < tamFiltro; i++)
    filtro[i] = 1.0;

}

/* OTRAS INICIALIZACIONES:  ENFOCADO  */
void inicializar_filtro_enfocado(float *filtro, int tamFiltro)
{
  filtro[0] = 0.0;
  filtro[1] = -1.0;
  filtro[2] = 0.0;
  filtro[3] = -1.0;
  filtro[4] = 5.0;
  filtro[5] = -1.0;
  filtro[6] = 0.0;
  filtro[7] = -1.0;
  filtro[8] = 0.0;

}





/***************************************************/
/* APLICACION DE FILTROS SECUENCIALES              */
/***************************************************/

/**
 Procesar cada pixel de la imagen. Para simplificar, no se procesan los bordes
 ya que el filtro utiliza los 8 vecinos.
*/
void filtro_sec_promedio(float *imagen_in, float *imagen_out, int filas, int cols, float *filtro)
{
  int i,j,k,l;
  float aux;

  for (i = 1; i < filas-1; i++) {
    for (j = 1; j < cols-1; j++){
      aux = 0.0;

      // aplicar el filtro al pixel [i,j].
      /*
       aux += filtro[0] * imagen_in[j-1 + (i-1) * X];
       aux += filtro[1] * imagen_in[j   + (i-1) * X];
       aux += filtro[2] * imagen_in[j+1 + (i-1) * X];

       aux += filtro[3] * imagen_in[j-1 + i * X];
       aux += filtro[4] * imagen_in[j   + i * X];
       aux += filtro[5] * imagen_in[j+1 + i * X];

       aux += filtro[6] * imagen_in[j-1 + (i+1) * X];
       aux += filtro[7] * imagen_in[j   + (i+1) * X];
       aux += filtro[8] * imagen_in[j+1 + (i+1) * X];
      */
      
      for(k=-1; k <= 1; k++) { // fila filtro
	for (l = -1; l <= 1; l++) {// col filtro
	  int fx = FILTRO_RCOLS + l;
	  int fy = FILTRO_RFILAS + k;
	  aux += filtro[fx + fy * FILTRO_COLS] * imagen_in[(j+l) + (i+k) * X];
	}
      }
	
      // modifico la imagen
      // al se el promedio, se debe dividir por 9
      imagen_out[j + i * X] = aux / TAM_FILTRO;
			
    }  // for columnas (j)
  } // for filas (i)

}


/* filtro secuencial enfocado*/
void filtro_sec_enfocado(float *imagen_in, float *imagen_out, int filas, int cols, float *filtro)
{
  int i,j,k,l;
  float aux;

  for (i = 1; i < filas-1; i++)
    for (j = 1; j < cols-1; j++){
      aux = 0.0;

      // aplico el filtro 8 vecinos	
      for(k=-1; k <= 1; k++) { // fila filtro
	for (l = -1; l <= 1; l++) {// col filtro
	  int fx = FILTRO_RCOLS + l;
	  int fy = FILTRO_RFILAS + k;
	  aux += filtro[fx + fy * FILTRO_COLS] * imagen_in[(j+l) + (i+k) * X];
	}
      }
      // modifico la imagen
      // no se debe dividir, solo se aplica el filtro
      imagen_out[j + i * X] = aux;
			
    }

}




/********************************************************************************/
/*                    FILTRO PARALELO                                    */
/********************************************************************************/


__global__ void kernel_aplicar_filtro_promedio(float *d_imagen_in,
					       float *d_imagen_out,
					       float *d_filtro,
					       int filas, int cols)
{

  // obtenger la fila y columna del thread que define el pixel a procesar
  int myCol = threadIdx.x + blockIdx.x * blockDim.x;
  int myRow = threadIdx.y + blockIdx.y * blockDim.y;

  // Procesar cada pixel de la imagen. Para simplificar, no se procesan los
  // bordes ya que el filtro utiliza los 8 vecinos 
  if ((myCol < cols-1) && (myRow < filas -1) && (myCol > 0) && (myRow > 0)) {

    int k, l;
    float aux = 0.0;


    // aplicar el filtro al pixel [myRow,myCol]
    for(k=-1; k <= 1; k++) { // fila filtro
      for (l = -1; l <= 1; l++) {// col filtro
	int fx = FILTRO_RCOLS + l;
	int fy = FILTRO_RFILAS + k;
	aux += d_filtro[fx + fy * FILTRO_COLS] *
	  d_imagen_in[(myCol+l) + (myRow+k) * X];
      }
    }

		
    // escribir la imagen de salida con el promedio
    d_imagen_out[myCol + myRow * cols] = aux / TAM_FILTRO;
    // d_imagen_out[myCol + myRow * cols] = aux * TAM_FILTRO_1;    
  }
}

/* filtro paralelo */
void filtro_par_promedio(float *d_imagen_in, float *d_imagen_out, int filas, int cols, float *d_filtro)
{
	
  // lanzamiento del kernel, se crea 1 thread por pixel de la imagen en una
  // grilla 2D que tiene el mismo tamaño que la imagen a procesar.
  dim3 nThreads(16,16);
  dim3 nBlocks(cols/nThreads.x + (cols % nThreads.x ? 1 : 0),
	       filas/nThreads.y + (filas % nThreads.y ? 1 : 0));

  // lanzamiento del kernel: 1 thread por pixel de la imagen
  kernel_aplicar_filtro_promedio<<<nBlocks, nThreads>>>
    (d_imagen_in, d_imagen_out, d_filtro, filas, cols);
    
  cudaDeviceSynchronize();

}

__global__ void kernel_aplicar_filtro_enfoque(float *d_imagen_in,
					      float *d_imagen_out,
					      float *d_filtro,
					      int filas, int cols)
{

  // obtenger la fila y columna del thread que define el pixel a procesar
  int myCol = threadIdx.x + blockIdx.x * blockDim.x;
  int myRow = threadIdx.y + blockIdx.y * blockDim.y;

  // Procesar cada pixel de la imagen. Para simplificar, no se procesan los
  // bordes ya que el filtro utiliza los 8 vecinos 
  if ((myCol < cols-1) && (myRow < filas -1) && (myCol > 0) && (myRow > 0)) {

    int k, l;
    float aux = 0.0;


    // aplicar el filtro al pixel [myRow,myCol]
    for(k=-1; k <= 1; k++) { // fila filtro
      for (l = -1; l <= 1; l++) {// col filtro
	int fx = FILTRO_RCOLS + l;
	int fy = FILTRO_RFILAS + k;
	aux += d_filtro[fx + fy * FILTRO_COLS] *
	  d_imagen_in[(myCol+l) + (myRow+k) * X];
      }
    }

		
    // escribir la imagen de salida con el promedio
    d_imagen_out[myCol + myRow * cols] = aux;
    // d_imagen_out[myCol + myRow * cols] = aux * TAM_FILTRO_1;    
  }
}

/* filtro enfocado */
void filtro_par_enfocado(float *d_imagen_in, float *d_imagen_out,
			int filas, int cols, float *d_filtro)
{
	
  // lanzamiento del kernel, se crea 1 thread por pixel de la imagen en una
  // grilla 2D que tiene el mismo tamaño que la imagen a procesar.
  dim3 nThreads(16,16);
  dim3 nBlocks(cols/nThreads.x + (cols % nThreads.x ? 1 : 0),
	       filas/nThreads.y + (filas % nThreads.y ? 1 : 0));

  // lanzamiento del kernel: 1 thread por pixel de la imagen
  kernel_aplicar_filtro_enfoque<<<nBlocks, nThreads>>>
    (d_imagen_in, d_imagen_out, d_filtro, filas, cols);
    
  cudaDeviceSynchronize();
}
