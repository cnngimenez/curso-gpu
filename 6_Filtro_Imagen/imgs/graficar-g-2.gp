reset

set title "Filtros de Imágenes"
set xtics 0,1
set xlabel "Experimento"
set ytics
set ylabel "Tiempo (ms)"
# set logscale y
set style data histograms
set style histogram cluster gap 1
set style fill solid 1.0
# set style fill transparent
# set xrange [0:3]

plot \
"results-g-2.csv" using "gpu":xtic(2) title "GPU"

pause 100
