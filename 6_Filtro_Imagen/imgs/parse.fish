#! /usr/bin/fish

set -l header "num tipo cpu gpu HtoD DtoH memcpy"

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    echo -n " "
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G"
set_color normal

echo "$header" > results-g.csv
for fout in ../results/filtro_*-G-summary-*.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-g.csv
end

