BEGIN {
    ORS=""
}

/Usando filtro / {
    match($0, "Usando filtro (.*)", a)
    salida[0] = a[1]
}

/Filtro -> / {
    match($0, /= (.*)/, a)
    split(a[1], b, " ")
    salida[1] = b[2]
    salida[2] = b[3]
}

ENDFILE {
    match(ARGV[ARGIND], "[[:digit:]]+", a)
    print a[0] " "
    print salida[0] " " salida[1] " "
    print salida[2]
}
