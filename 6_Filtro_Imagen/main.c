#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime_api.h>
#include <cassert>

#include "defs.h"
#include "imagen.h"
#include "../common/check.h"
#include "../common/cpu_timer.h"
#include "../common/gpu_timer.h"

int main(int argc, char **argv){
  // --- Imprimir detalles de la placa
  int card;
  cudaGetDevice(&card);
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, card);
  printf("\nDevice %d: \"%s\" \n", card, deviceProp.name);

#ifdef FENFOCADO
  printf("Usando filtro ENFOCADO\n");
#else
  printf("Usando filtro PROMEDIO\n");
#endif
  
  // int size = X * Y * sizeof(float);

  /* Nombres de archivos que se usan si SALVAR_IMAGEN = 1. Se usan luego para
    ver los resultados */
#ifdef FENFOCADO
  const char OUTPUT_SEC[] = "output_enfocado_sec.txt";
  const char OUTPUT_PAR[] = "output_enfocado_par.txt";
#else
  const char OUTPUT_SEC[] = "output_paralelo_sec.txt";
  const char OUTPUT_PAR[] = "output_paralelo_par.txt";
#endif


  // Reservar memoria en host
  float *h_imagen_in, *h_imagen_out, *h_filtro, *imagen_out_check;
  h_imagen_in = (float *) malloc(SIZE_IMG);
  h_imagen_out = (float *) malloc(SIZE_IMG);
  h_filtro = (float *) malloc(SIZE_FILTRO);
  // se usa para comprar resultados
  imagen_out_check = (float *) malloc(SIZE_IMG);

  // Reservar memoria en device 
  float *d_imagen_in, *d_imagen_out, *d_filtro;
  cudaMalloc((void **) &d_imagen_in, SIZE_IMG);
  cudaMalloc((void **) &d_imagen_out, SIZE_IMG);
  cudaMalloc((void **) &d_filtro, SIZE_FILTRO);

  check_errors("cuda malloc");

  if (!h_imagen_in || !h_imagen_out || !h_filtro || !imagen_out_check
      || !d_imagen_in || !d_imagen_out || !d_filtro ) {
    printf("No aloca memoria para la imagen o filtro \n");
    exit(-1);
  }

  
  /* lectura de la imagen a procesar  */
  leer_imagen(INPUT, h_imagen_in, Y, X);
  printf("Imagen leída\n");

#ifdef FENFOCADO
  /* Inicializacion del filtro en host. Todos 1s*/
  inicializar_filtro_enfocado(h_filtro, TAM_FILTRO);
  printf("Filtro ENFOCADO incializado\n");
#else
  /* Inicializacion del filtro en host. Todos 1s*/
  inicializar_filtro_promedio(h_filtro, TAM_FILTRO);
  printf("Filtro PROMEDIO incializado\n");
#endif
  
  // copiar imagen y filtro desde host a device.

  gpu_timer crono_htod;
  crono_htod.tic();
  cudaMemcpy(d_imagen_in, h_imagen_in, SIZE_IMG, cudaMemcpyHostToDevice);
  cudaMemcpy(d_filtro, h_filtro, SIZE_FILTRO, cudaMemcpyHostToDevice);
  crono_htod.tac();
  
  check_errors("memcpy HtoD");

  printf("host to device memcpy time: %lf\n",
	 crono_htod.ms_elapsed);

  
  /* Solucion secuencial */
  cpu_timer crono_cpu; 
  crono_cpu.tic();
#ifdef FENFOCADO
  filtro_sec_enfocado(h_imagen_in, imagen_out_check, Y, X, h_filtro);
#else
  filtro_sec_promedio(h_imagen_in, imagen_out_check, Y, X, h_filtro);
#endif
  crono_cpu.tac();

  /* si SALVAR_IMAGEN es 1 se guarda en disco la imagen */
  if (SALVAR_IMAGEN) 
    salvar_imagen(OUTPUT_SEC, imagen_out_check, Y,X);

  printf("Filtro secuencial aplicado\n");

 
  /*  Solucion paralela */
  gpu_timer crono_gpu;
  crono_gpu.tic();
#ifdef FENFOCADO
  filtro_par_enfocado(d_imagen_in, d_imagen_out, Y, X, d_filtro);
#else
  filtro_par_promedio(d_imagen_in, d_imagen_out, Y, X, d_filtro);
#endif
  crono_gpu.tac();

  check_errors("filtro_par_promedio");

  
  // traer los datos desde device a host usando h_imagen_out.
  gpu_timer crono_dtoh;
  crono_dtoh.tic();
  cudaMemcpy(h_imagen_out, d_imagen_out, SIZE_IMG, cudaMemcpyDeviceToHost);
  crono_dtoh.tac();
  check_errors("h_imagen_out <- d_imagen_out memcpy");

  printf("h_imagen_out <- d_imagen_out memcpy time: %lf\n",
	 crono_dtoh.ms_elapsed);

  /* si SALVAR_IMAGEN es 1 se guarda en disco la imagen */
  if (SALVAR_IMAGEN)
    salvar_imagen(OUTPUT_PAR, h_imagen_out, Y , X);

  printf("Filtro -> [(Filas x Columnas) imagen /ms_cpu/ms_gpu]= (%dx%d) %lf %lf\n",
	 Y,X, crono_cpu.ms_elapsed, crono_gpu.ms_elapsed);

  /* Comparacion */	
  /* no proceso los bordes ya que no les aplico el filtro */
  for (int i = 1; i < (Y-1); i++)
    for (int j = 1; j < (X-1); j++){
      assert(h_imagen_out[i * X + j] == imagen_out_check[i * X + j]);
    }
	

  // desalocar memoria en host y device de todos los arreglos.
  free(h_imagen_in);
  free(h_imagen_out);
  free(h_filtro);
  free(imagen_out_check);

  cudaFree(d_imagen_in);
  cudaFree(d_imagen_out);
  cudaFree(d_filtro);

  check_errors("cuda free");
		
  return 0;
}
