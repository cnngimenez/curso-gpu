#! /bin/sh

# Enqueue for all groups
alias mrun='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-26] --gres=gpu:1'
# Enqueue for Group G
alias mrun_g='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[03-28] --gres=gpu:1'
# Enqueue for Group T
alias mrun_t='srun -p qCDER -N1 -n1 -c1 --exclusive --exclude=cder[01-26] --gres=gpu:1'

# Parameters:
#
#     runall command parameters number-test
#
function runall {
    mrun_g nvprof --metrics all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-metrics-$3.err" &
    mrun_g nvprof --events all ./$1 $2 > "results/$1-G-metrics-$3.out" 2> "results/$1-G-events-$3.err" &
    mrun_g nvprof ./$1 $2 > "results/$1-G-summary-$3.out" 2> "results/$1-G-summary-$3.err" &
    
    
    mrun_t nvprof --metrics all ./$1 $2 > "results/$1-T-metrics-$3.out" 2> "results/$1-T-metrics-$3.err" &
    mrun_t nvprof --events all ./$1 $2 > "results/$1-T-events-$3.out" 2> "results/$1-T-events-$3.err" &
    mrun_t nvprof ./$1 $2 > "results/$1-T-summary-$3.out" 2> "results/$1-T-summary-$3.err" &
    
}

runall "filtro_promedio" "" 1
runall "filtro_enfocado" "" 2
