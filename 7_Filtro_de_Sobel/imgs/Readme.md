# Usage

Execute `source parse.fish`. This will create the CSV.

# Parse.fish

This script parse the out files for the cpu and gpu times. Also it search for the memcpy times on the err files reported by the `nvprof` program.
