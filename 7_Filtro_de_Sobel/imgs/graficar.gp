reset

set title "Filtro Sobel"
set xtics 0,1
set xlabel "Experimento"
set ytics
set ylabel "Tiempo (ms)"
# set logscale y
set style data histograms
set style histogram cluster gap 1
set style fill solid 1.0
# set style fill transparent
# set xrange [0:3]

plot \
"results-g-2.csv" using "cpu" title "CPU.", \
"results-g-2.csv" using "total_gpu" title "GPU + memcpy", \
"results-g-2.csv" using "memcpy" title "memcpy", \
"results-g-2.csv" using "gpu" title "GPU", \
"results-t.csv" using "cpu" title "CPU T.", \
"results-t.csv" using "gpu" title "GPU T"
pause 100
