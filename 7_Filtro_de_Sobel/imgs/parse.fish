#! /usr/bin/fish

set -l header "cpu gpu HtoD DtoH memcpy"

function parse-times
    gawk -f tiempos.awk  -- $argv[1]
    echo -n " "
    gawk -f tiempos2.awk -- $argv[2]
    echo 
end 

set_color --bold
echo "Group G"
set_color normal

echo "$header" > results-g.csv
for fout in ../results/sobel-G-summary-.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-g.csv
end

set_color --bold
echo "Group T"
set_color normal

echo "$header" > results-t.csv
for fout in ../results/sobel-T-summary-.out
    set -l fbasename (basename --suffix .out "$fout")
    parse-times "$fout" ../results/"$fbasename".err >> results-t.csv
end

# ############################################################################

set_color --bold
echo "Group G - Metrics"
set_color normal

# Metrics
echo "\"Test Number\" \"Invocations\" \"Metric Name\" \"Metric Description\" \"Min\" \"Max\" \"Avg\" \"Kernel\"" > csv/results-metrics.csv
for fout in ../results/sobel-G-metrics-*.err
    gawk -f metrics.awk -- "$fout" >> csv/results-metrics.csv
end

# ############################################################################

set_color --bold
echo "Group G - Events"
set_color normal

echo "\"Test Number\" \"Invocations\" \"Event Name\" \"Min\" \"Max\" \"Avg\" \"Total\" \"Kernel\"" > csv/results-events.csv
for fout in ../results/sobel-G-events-*.err
    gawk -f events.awk -- "$fout" >> csv/results-events.csv
end

# ############################################################################

function parse-metric
    echo "$argv[1]"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-gm.csv"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-cm.csv"
    head -n 1 csv/results-metrics.csv > "csv/$argv[1]-sm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep 'conv_gpu$'  >> "csv/$argv[1]-gm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep "conv_gpu_constant_memory"  >> "csv/$argv[1]-cm.csv"
    grep "$argv[1]" csv/results-metrics.csv | grep "conv_gpu_shared_memory"  >> "csv/$argv[1]-sm.csv"
end

set_color --bold
echo "Group G - Selected Metrics"
set_color normal

parse-metrics "branch_efficiency"
