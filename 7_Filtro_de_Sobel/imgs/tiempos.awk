BEGIN {
    ORS=""
}

/Sobel -> / {
    match($0, /= (.*)/, a)
    split(a[1], b, " ")
    salida[0] = b[2]
    salida[1] = b[3]
}

ENDFILE {
    print salida[0] " " salida[1] 
}
