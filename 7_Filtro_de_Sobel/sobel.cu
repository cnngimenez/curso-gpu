#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cassert>

#include "../common/check.h"
#include "../common/cpu_timer.h"
#include "../common/gpu_timer.h"

#include "imagen.h"

#define Y 750
#define X 499
#define TAM_FILTRO 9

#define NAME "Antonov.txt"

#define SALVAR_IMAGEN 1

/********************************************************************************/
/* 				PARALELO					*/
/********************************************************************************/

__constant__ float d_filtro_hor[9];
__constant__ float d_filtro_ver[9];

__global__ void kernel_aplicar_filtro(float* d_imagen_in, float* d_imagen_out,
				      bool hor, int filas, int cols)
{

  int icol = threadIdx.x + blockIdx.x * blockDim.x; 
  int irow = threadIdx.y + blockIdx.y * blockDim.y;


  if ((icol < cols-1) && (irow < filas-1)
      && (icol > 0) && (irow > 0)){
    float aux = 0.0;

    // Aplicar el filtro al pixel [icol, irow] de d_imagen_in
    // k y l son desplazamientos de rango entre -1..1
    for (int k = -1; k <= 1; k++){ // filas
      for (int l = -1; l <= 1; l++){ // cols
	int fx = l + 1; // X del filtro
	int fy = k + 1; // Y del filtro

	// No se espera que surgan dos ramificaciones puesto que "hor" va a ser
	// true o false para los 32 threads.
	if (hor){
	  aux += d_filtro_hor[fx + fy * 3] *
	    d_imagen_in[(icol + l) + (irow + k) * cols];
	}else{
	  aux += d_filtro_ver[fx + fy * 3] *
	    d_imagen_in[(icol + l) + (irow + k) * cols];
	}
	  
      }
    }

    d_imagen_out[icol + irow * cols] = aux;
  }
}



/**
 calcula la salida G (magnitud del gradiente)
*/
__global__ void kernel_calcular_g(float *d_g_x, float *d_g_y,
				  float *d_imagen_out,
				  int filas, int cols)
{

  int icol = threadIdx.x + blockIdx.x * blockDim.x;
  int irow = threadIdx.y + blockIdx.y * blockDim.y;

  if ((icol < cols-1) && (irow < filas-1)
      && (icol > 0) && (irow > 0)){

    int idx = icol + irow * cols;
    d_imagen_out[idx] = (float) sqrt
      (
       (float) powf(d_g_x[idx], 2)
       + (float) powf (d_g_y[idx], 2)
       );
  }
}

/* SOBEL paralelo */
void aplicar_filtro_sobel_par(float *d_imagen_in, float *d_imagen_out,
			      int filas, int cols)
{
  // tomando de guia la soluciona secuencial paralelizar usando GPU
  const int size_img = filas * cols * sizeof(float);
  
  // Inicializar filtros
  float* h_filtro_hor = (float *) malloc(sizeof(float) * 9);
  float* h_filtro_ver = (float *) malloc(sizeof(float) * 9);

  assert(h_filtro_hor);
  assert(h_filtro_ver);

  inicializar_filtro_sobel_horizontal(h_filtro_hor, 9);
  inicializar_filtro_sobel_vertical(h_filtro_ver, 9);
  
  check_errors("cuda mallocs: filtros");

  // Aprovechemos la memoria constante
  cudaMemcpyToSymbol(d_filtro_hor, h_filtro_hor, sizeof(float) * 9);
  check_errors("d_filtro_hor <- h_filtro_hor memcpy");
  cudaMemcpyToSymbol(d_filtro_ver, h_filtro_ver, sizeof(float) * 9);
  check_errors("d_filtro_ver <- h_filtro_ver memcpy");

  
  // Resultados
  float *d_g_x, *d_g_y;
  cudaMalloc((void **) &d_g_x, size_img);
  cudaMalloc((void **) &d_g_y, size_img);

  check_errors("cuda mallocs: g_xy");


  // Definimos la "grilla" y los hilos.
  dim3 blocklayout(16, 16);
  dim3 gridlayout(cols /blocklayout.x + (cols  % blocklayout.x ? 1 : 0),
		  filas/blocklayout.y + (filas % blocklayout.y ? 1 : 0));

  
  // Aplicamos Gx y Gy
  kernel_aplicar_filtro<<<gridlayout, blocklayout>>>
    (d_imagen_in, d_g_x, true, filas, cols);
  check_errors("aplicar g_x");
  
  kernel_aplicar_filtro<<<gridlayout, blocklayout>>>
    (d_imagen_in, d_g_y, false, filas, cols);
  check_errors("aplicar g_y");

  // Calculamos G
  kernel_calcular_g<<<gridlayout, blocklayout>>>
    (d_g_x, d_g_y, d_imagen_out, filas, cols);
  check_errors("calcula g");


  // Liberamos memoria
  free(h_filtro_hor);
  free(h_filtro_ver);
    
  /*
   cudaFree(d_filtro_hor);
   cudaFree(d_filtro_ver);
  */
  cudaFree(d_g_x);
  cudaFree(d_g_y);
  check_errors("cuda free");

}


int main(){

  int size = X * Y * sizeof(float);

  /* Alocacion de memoria en host */
  float *h_imagen_in, *h_imagen_out, *imagen_out_check;
  h_imagen_in = (float*) malloc(size);
  h_imagen_out = (float*) malloc(size);
  imagen_out_check =(float*)malloc(size);

  const char OUTPUT_SEC[] = "output_sec.txt";
  const char OUTPUT_PAR[] = "output_par.txt";

  /* Alocacion de memoria en device */
  float *d_imagen_in, *d_imagen_out;
  cudaMalloc((void**)&d_imagen_in, size);
  cudaMalloc((void**)&d_imagen_out, size);
  check_errors("cuda malloc");


  if (!h_imagen_in || !h_imagen_out || !d_imagen_in || !d_imagen_out  ) {
    printf("No aloca memoria para la imagen o filtro \n");
    exit(-1);
  }


  /* LECTURA DE IMAGEN A PROCESAR */
  leer_imagen(NAME, h_imagen_in, Y, X);
  /* copia de datos desde CPU a GPU: imagen original y filtro*/
  gpu_timer crono_htod;
  crono_htod.tic();
  cudaMemcpy(d_imagen_in, h_imagen_in, size, cudaMemcpyHostToDevice);
  crono_htod.tac();
  check_errors("d_imagen_in <- h_imagen_in memcpy");


  /* FILTRO SOBEL SECUENCIAL */
  cpu_timer crono_cpu;
  crono_cpu.tic();
  aplicar_filtro_sobel_sec(h_imagen_in, imagen_out_check, Y, X);
  crono_cpu.tac();

  if (SALVAR_IMAGEN)
    salvar_imagen(OUTPUT_SEC, imagen_out_check, Y, X);


  /* FILTRO SOBEL PARALELO*/
  gpu_timer crono_gpu;
  crono_gpu.tic();
  aplicar_filtro_sobel_par(d_imagen_in, d_imagen_out, Y, X);
  crono_gpu.tac();
  check_errors("filtro_sobel");

  gpu_timer crono_dtoh;
  crono_dtoh.tic();
  cudaMemcpy(h_imagen_out, d_imagen_out, size, cudaMemcpyDeviceToHost);
  crono_dtoh.tac();
  check_errors("h_imagen_out <- d_imagen_out memcpy");

  if (SALVAR_IMAGEN)
    salvar_imagen(OUTPUT_PAR, h_imagen_out, Y,X);


  printf("Sobel -> [(Filas x Columnas) imagen /ms_cpu/ms_gpu]= (%dx%d) %lf %lf\n",
	 Y,X, crono_cpu.ms_elapsed, crono_gpu.ms_elapsed);

  /* Comparacion */	
  /* no proceso los bordes ya que no les aplico el filtro */
  printf("-- Comparación:\n");
  printf("leyenda: col, fila (arr_index) gpu_out != cpu_out\n");
  double epsilon = 1.0E-4;
  for (int f = 1; f < (Y-1); f++){ // filas
    for (int c = 1; c < (X-1); c++){ // cols
      // assert(h_imagen_out[i * X + j] == imagen_out_check[i * X + j]);
      int ind = f * X + c;
      
      if (abs(h_imagen_out[ind] - imagen_out_check[ind]) > epsilon){
	printf("\ndiferencia: %d, %d (%d)  %lf != %lf (%lf)",
	       c, f, ind,
	       h_imagen_out[ind], imagen_out_check[ind],
	       abs(h_imagen_out[ind]-imagen_out_check[ind]));
      }
    }
  }
  

  free(h_imagen_in);
  free(h_imagen_out);
  free(imagen_out_check);

  cudaFree(d_imagen_in);
  cudaFree(d_imagen_out);
  check_errors("cudaFree");

  return 0;
}
