- Versión Web: http://crowd.fi.uncoma.edu.ar/~christian.gimenez/cursos/gpu/
- Descargar Versión PDF: https://bitbucket.org/cnngimenez/curso-gpu/downloads/

# Cómputo de Alto Rendimiento en GPU
Trabajo para el Curso "Cómputo de Alto Rendimiento en GPU" dictado por la Dra. Mónica Denham.

Abril, 2019.

# Herramientas Utilizadas
Para realizar el informe se utilizó [Emacs Org-mode](https://orgmode.org/). Éste genera código LaTeX para producir los archivos PDF y HTML.

Versionador utilizado: [Mercurial](https://www.mercurial-scm.org/). 

# Versión Web 
Se puede ver una versión Web del informe y de los archivos Orgs en http://crowd.fi.uncoma.edu.ar/~christian.gimenez/cursos/gpu/

Los archivos HTML también pueden ser visualizados de forma local luego de clonar el repositorio. 
