#ifndef _CHECK_H
#define _CHECK_H 1
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

/**
 Return if there's an error.

 Exit program if error is not cudaSuccess.
 */
void check_errors(const char* mesg){
  cudaError_t err = cudaGetLastError();
  
  if (err != cudaSuccess){
    printf(mesg);
    printf(cudaGetErrorName(err));
    printf("\n");
    printf(cudaGetErrorString(err));
    printf("\n");
    exit(1);
  }
    
}

#endif /* _CHECK_H */
