Curso de *Computación de Alto Rendimiento en GPU*.

Abril del 2019.

[[file:TP.org][Informe del Trabajo Práctico]] 

[[file:OpenCL.org][Implementación de los ejercicios en OpenCL]]

[[file:Vulkan.org][Implementación de los ejercicios en Vulkan]]


* Meta     :noexport:

  # ----------------------------------------------------------------------
  #+TITLE: Computación de Alto Rendimiento en GPU
  #+SUBTITLE: Página Principal
  #+AUTHOR:    Gimenez, Christian
  #+DATE:      25 abr 2019
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:libs/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+XSLT:
  #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
  #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
  #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>
  #+LANGUAGE: es


  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # End:
