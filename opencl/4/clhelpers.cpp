/* 
   Copyright 2019 Christian Gimenez
   
   Author: Christian Gimenez

   clhelpers.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

#if !defined(CL_CALLBACK)
#define CL_CALLBACK
#endif

//#include "clhelpers.hpp"

void CL_CALLBACK contextCallback(const char *errInfo, const void *private_inde,
				 size_t cb, void *user_data){
  std::cerr << "Error occured during context use: " << errInfo << std::endl;
  exit(1);
}

void report_create_kernel_err(cl_int errNum){
  std::cout << "Create Kernel Error (" << errNum << "): " << std::endl;
  switch (errNum){
  case CL_INVALID_PROGRAM:
    std::cout << "Invalid program" << std::endl;
    break;
  case CL_INVALID_PROGRAM_EXECUTABLE:
    std::cout << "Invalid program executable" << std::endl;
    break;
  case CL_INVALID_KERNEL_NAME:
    std::cout << "Invalid kernel name" << std::endl;
    break;
  case CL_INVALID_KERNEL_DEFINITION:
    std::cout << "Invalid kernel definition" << std::endl;
    break;
  case CL_INVALID_VALUE:
    std::cout << "Invalid value" << std::endl;
    break;
  case CL_OUT_OF_HOST_MEMORY:
    std::cout << "Out of host memory" << std::endl;
    break;
  default:
    std::cout << "Unknown error" << std::endl;
  }
}

void print_dev_info(cl_device_id dev_id,
		    cl_device_info name,
		    std::string str){
  size_t size;
  
  clGetDeviceInfo(dev_id, name, 0, NULL, &size);
  char *info = (char*) alloca( sizeof(char) * size);
  clGetDeviceInfo(dev_id, name, size, info, NULL);

  std::cout << str << ": ";
  if (name == CL_DEVICE_TYPE){
    cl_device_type t = *(reinterpret_cast<cl_device_type*>(info));

    switch (t){
    case CL_DEVICE_TYPE_CPU:
      std::cout << "CPU" << std::endl;
      break;
    case CL_DEVICE_TYPE_GPU:
      std::cout << "GPU" << std::endl;
      break;
    case CL_DEVICE_TYPE_ACCELERATOR:
      std::cout << "Accelerator" << std::endl;
      break;
    case CL_DEVICE_TYPE_DEFAULT:
      std::cout << "Default" << std::endl;
      break;
    }

  }else{
    std::cout << info << std::endl;
  }
}

void print_platform_info(cl_platform_id platform_id,
			 cl_platform_info name,
			 std::string str){
  cl_uint err;
  std::size_t size;
  
  err = clGetPlatformInfo(platform_id, name, 0, NULL, &size);
  if (err != CL_SUCCESS){
    std::cout << "Cannot retrieve " << str << std::endl;
    return;
  }
  char *info = (char*) alloca( sizeof(char) * size);
  err = clGetPlatformInfo(platform_id, name, size, info, NULL);
  if (err != CL_SUCCESS){
    std::cout << "Cannot retrieve " << str << std::endl;
    return;
  }
  
  std::cout << str << ": " << info << std::endl;
}

void print_platforms(cl_platform_id* platformsIDs, cl_uint numPlatforms){
  std::cout << "Platforms" << std::endl;
  for (cl_uint i = 0; i < numPlatforms; i++){
    print_platform_info(platformsIDs[i], CL_PLATFORM_NAME, "Name");
    print_platform_info(platformsIDs[i], CL_PLATFORM_VENDOR, "Vendor");
    std::cout << std::endl;
  }
}

void print_context_info(cl_platform_id id,
			int numDevices,
			cl_device_id *deviceIDs){
  size_t size;

  std::cout << "Context:" << std::endl;
  std::cout << "Platform selected: " << id << std::endl;
  
  print_platform_info(id, CL_PLATFORM_NAME, "Name");
  print_platform_info(id, CL_PLATFORM_VENDOR, "Vendor");
  
 
  std::cout << "Devices selected: " << std::endl;
  for (int j = 0; j < numDevices; j++){
    std::cout << "ID:" <<  deviceIDs[j] << std::endl;
    print_dev_info(deviceIDs[j], CL_DEVICE_TYPE, "Type");
    print_dev_info(deviceIDs[j], CL_DEVICE_NAME, "Name");
    print_dev_info(deviceIDs[j], CL_DEVICE_VENDOR, "Vendor");
    print_dev_info(deviceIDs[j], CL_DEVICE_VERSION, "Dev. Version");
    print_dev_info(deviceIDs[j], CL_DRIVER_VERSION, "Driver Version");
    
  }
  std::cout << std::endl; 
}

void all_platforms(cl_platform_id **platform_ids, cl_uint *num_platforms){
  cl_int errNum;
  cl_uint numPlatforms;
  cl_platform_id *platformIDs;

  errNum = clGetPlatformIDs(0, NULL, &numPlatforms);

  if (errNum != CL_SUCCESS) {
    printf("clGetPlatformIDs error");
    exit(1);
  }
  
  platformIDs = (cl_platform_id *) malloc
    (sizeof(cl_platform_id) * numPlatforms);

  errNum = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
  if (errNum != CL_SUCCESS){
    printf("Couldn't get platforms IDs");
    exit(1);
  }

  *num_platforms = numPlatforms;
  *platform_ids = platformIDs;
}

bool select_platform(const char* name,
		     cl_platform_id* platformIDs,
		     cl_uint numPlatforms,
		     cl_platform_id *selected_id){

  bool founded = false;
  
  for (cl_uint i = 0; i < numPlatforms; i++){
    std::size_t size;
    clGetPlatformInfo(platformIDs[i], CL_PLATFORM_NAME, 0, NULL, &size);
    
    char *info = (char*) alloca( sizeof(char) * size );
    clGetPlatformInfo(platformIDs[i], CL_PLATFORM_NAME, size, info, NULL);

    if (strcmp(info, name) == 0){
      *selected_id = platformIDs[i];
      founded = true;
    }
  }
  
  return founded;
}

/**
 Search for devices through all the platforms.
 */
void select_devices(cl_platform_id selected_platform,
		    cl_device_type type,
		    cl_device_id **device_ids,
		    cl_uint* num_devices,
		    cl_uint* platformI){
  cl_uint i, numDevices;
  cl_int errNum;
  cl_device_id *deviceIDs;

  // Search for the amount of devices in the platform.
  errNum = clGetDeviceIDs(selected_platform, type, 0, NULL, &numDevices);
  *num_devices = numDevices;

  if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND){
    std::cerr << "Device not found or error";
    exit(1);
  }else if (numDevices > 0){

    // Get all device IDs from that platform.
    deviceIDs = (cl_device_id *) malloc(sizeof(cl_device_id) * numDevices);
    *device_ids = deviceIDs;
    
    errNum = clGetDeviceIDs(selected_platform, type,
			    numDevices, &deviceIDs[0], NULL);
    *platformI = i;
  }  
}

void create_context(cl_platform_id platform_id,
		    cl_device_id *deviceIDs,
		    cl_uint numDevices,
		    cl_context *context){
  cl_int errNum;
  cl_context_properties contextProps[] =
    {
     // Specifies the platform to use in the next element
     // Which platform to use
     CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id,
     0  // 0 ends the context properties array (like C strings)
    };

  *context = clCreateContext(contextProps,
			     // devices attached to this context
			     numDevices, deviceIDs,
			     // Callbacks and error reporting
			     &contextCallback, NULL, &errNum);
  if (errNum != CL_SUCCESS){
    std::cerr << "clCreateContext Error" << std::endl;
    exit(1);
  }
}

void load_code(const char* filename, std::string *src){
  std::ifstream srcFile(filename);
  
  if (!srcFile.is_open()){
    std::cerr << "Error: convolucion.cl not opened" << std::endl;
    exit(1);
  }

  std::string srcProg(std::istreambuf_iterator<char>(srcFile),
		      (std::istreambuf_iterator<char>()) );

  *src = srcProg;
}

void compile_code(cl_context context,
		  cl_uint numDevices,
		  cl_device_id *deviceIDs,
		  std::string src,
		  cl_program *program){
  cl_int errNum;

  const char* csrc = src.c_str();
  size_t length = src.length();
  
  *program = clCreateProgramWithSource(context, 1, &csrc, &length, &errNum);

  if (errNum != CL_SUCCESS){
    std::cerr << "CreateProgram error:" << std::endl;
    switch (errNum){
    case CL_INVALID_CONTEXT:
      std::cerr << "Invalid Context" << std::endl;
      break;
    case CL_INVALID_VALUE:
      std::cerr << "Invalid Value" << std::endl;
      std::cerr << "Length: " << length << std::endl;
      std::cerr << "Source: " << std::endl;      
      std::cerr << csrc << std::endl;
      break;
    case CL_OUT_OF_HOST_MEMORY:
      std::cerr << "Out of host memory" << std::endl;
      break;
    default:
      std::cerr << "Unknown error" << std::endl;
    }
    exit(1);
  }
  
  errNum = clBuildProgram(*program, numDevices, deviceIDs, NULL, NULL, NULL);

  if (errNum != CL_SUCCESS){
    char buildLog[16384];
    clGetProgramBuildInfo(*program, deviceIDs[0], CL_PROGRAM_BUILD_LOG,
			  sizeof(buildLog), buildLog, NULL);
    std::cerr << "Compile error in kernel: " << std::endl;
    std::cerr << buildLog;
    exit(1);
  }
}

void create_kernel(cl_program program,
		   const char* kernel_name,
		   cl_kernel *kernel){
  cl_int errNum;
  *kernel = clCreateKernel(program, kernel_name, &errNum);
  
  if (errNum != CL_SUCCESS){
    report_create_kernel_err(errNum);
    exit(1);
  }
}
