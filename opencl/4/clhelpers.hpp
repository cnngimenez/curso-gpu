/* 
   Copyright 2019 Christian Gimenez
   
   Author: Christian Gimenez

   clhelpers.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLHELPERS_HPP
#define _CLHELPERS_HPP 1

#include <string>

using namespace std;

/**
 Show in stdout a string that represent the Create Kernel Error.

 @param errnum cl_int A CL error number returned by clCreateKernel.
 */
void report_create_kernel_err(cl_int errNum);

/**
 Print a particular device information value to stdout.

 Search for the value of a given field and print it to stdout.

 @param name The name of the CL field. 
 @param str A label to print before the value.
 */
void print_dev_info(cl_device_id dev_id,
		    cl_device_info name,
		    std::string str);

/**
 Print a particular platform information value to stdout.

 Search for the value of a given field and print it to stdout.

 @param str A label to print before de value.
 */
void print_platform_info(cl_platform_id platform_id,
			 cl_platform_info name,
			 std::string str);

/**
 Print all platform information.
 
 @param platformIDs An array of platforms ids.
 @param numPlatforms The amount of platforms in the array.
 */
void print_platforms(cl_platform_id* platformIDs, cl_uint numPlatforms);

/**
 Print the context information.

 Print the platform selected and the devices used for the context.
 */
void print_context_info(cl_platform_id id,
			int numDevices,
			cl_device_id *deviceIDs);

/**
 Return all the platforms identified by OpenCL.
 
 @param platform_ids Output. An array with platforms IDs.
 @param num_platforms Output. The amount of platforms founded.
 */
void all_platforms(cl_platform_id **platform_ids, cl_uint *num_platforms);

/**
 Search for the platform name and return its ID.

 @param name The platform name to search.
 @param platformIDs The IDs of the platform to search within.
 @param numPlatforms The amount of platforms IDs.
 @param selected_id Output. The platform ID with the same name as the given.
 @return true if founded, false otherwise.
 */
bool select_platform(const char* name,
		     cl_platform_id* platformIDs,
		     cl_uint numPlatforms,
		     cl_platform_id *selected_id);

void select_devices(cl_platform_id selected_platform,
		    cl_device_type type,
		    cl_device_id **device_ids,
		    cl_uint* num_devices,
		    cl_uint* platformI);

void create_context(cl_platform_id platform_id,
		    cl_device_id *deviceIDs,
		    cl_uint numDevices,
		    cl_context *context);

void load_code(const char* filename, string* src);

void compile_code(cl_context context,
		  cl_uint numDevices,
		  cl_device_id *deviceIDs,
		  string src,
		  cl_program *program);

void create_kernel(cl_program program,
		   const char* kernel_name,
		   cl_kernel *kernel);

#endif /* _CLHELPERS_HPP */
