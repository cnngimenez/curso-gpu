/* 

   Copyright 2019 Christian Gimenez

   Author: Christian Gimenez

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

__kernel void convolucion_gpu
(
 __constant float const *input,
 __global   float *output,
 __constant float const *filter,
 const int n, // Input size
 const int m // Filter size
 ){
  int j = get_global_id(0);
  // int j = (blockIdx.x * blockDim.x) + threadIdx.x ;
  	
  /*
   Barro vector input (tamaño N) y para cada elemento j hasta N hago la 
   operacion de convolucion: elemento i del vector filter por elemento 
   i+j del vector input.
  */
  if (j < n){
    output[j] = 0.0;
    for(int i = 0; i < m; i++){
      output[j] += filter[i] * input[i+j];
    }
  }
}
