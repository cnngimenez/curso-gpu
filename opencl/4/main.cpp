/* 

   Copyright 2019 Christian Gimenez

   Author: Christian Gimenez

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <string>
#include <ctime>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "cpu_timer.h"

#include "clhelpers.hpp"

void message(const char* str){
  std::cout << "-> " << str << std::endl;
}

void init_input(float* input, size_t size){
  for (int i = 0; i < size; i++){
    input[i] = (float) (rand() % 100);
  }
}

void setup_filter(float* filter, size_t size){
  for(int i = 0; i < size; i++) {
    // filter[i] = (float) (rand() % 100);
    filter[i] = 1;
  }
}

/* Convolucion en la cpu */
void convolucion_cpu(const float* input, float* output, const float* filter,
		     const int n, const int m) 
{
  /*
   Ayuda: se implementa la convolucion secuencial. 
   Tenga en cuenta que esta es una posible solución muy simple al 
   problema.
  */
  float temp;
	
  /*
   Barrido del vector input (tamaño N) y para cada elemento j hasta N 
   hago la operacion de convolucion: elemento i del vector filter por
   elemento i+j del vector input.
  */
  for(int j = 0; j < n; j++){	
    temp = 0.0;
    for(int i = 0; i < m; i++){
      temp += filter[i]*input[i+j];
    }
    output[j] = temp;
  }

}

int main(int argc, char** argv){

  /* --- Parse parameters --- */
  if (argc < 2){
    printf("Synopsis: ./convolucion_float array_size filter_size");
    exit(1);
  }

  const unsigned int n = atoi(argv[1]); // array size
  const unsigned int m = atoi(argv[2]); // filter size
  const unsigned int size_input = sizeof(cl_float) * (n + m);
  const unsigned int size_filter = sizeof(cl_float) * m;
  const unsigned int size_output = sizeof(cl_float) * n;

// chequeo que las dimensiones N y M sean correctas para esta solucion
assert((n % m == 0) && (m < 1024));

cl_int errNum;

// Select OpenCL platforms to run on 
cl_platform_id* platformIDs = NULL;
cl_platform_id selected_platform;
cl_uint numPlatforms;
all_platforms(&platformIDs, &numPlatforms);

print_platforms(platformIDs, numPlatforms);
message("Platforms detected");

// Clover == MESA
std::string platform_name = "Clover";

bool errb = select_platform(platform_name.c_str(), platformIDs, numPlatforms,
			    &selected_platform);

assert(errb);

// Iterate through the list of platforms until we find one that supports
// a GPU device, otherwise fail with an error.
cl_device_id *deviceIDs = NULL;
cl_uint numDevices;
cl_uint platform_index;
select_devices(selected_platform,
	       CL_DEVICE_TYPE_GPU,
	       &deviceIDs, &numDevices, &platform_index);

print_context_info(platformIDs[platform_index], numDevices, deviceIDs);

message("Devices detected");

// Create OpenCL context for creating buffers
// We need the device and the platform for creating a new context.

// No special properties is going to be used.
cl_context context;
create_context(selected_platform,
	       deviceIDs,
	       numDevices,
	       &context);

message("Context created");

// Load OpenCL code
string src;
size_t length;
load_code("convolucion.cl", &src);

message("CL Code loaded");

// Compile OpenCL code
cl_program program;
compile_code(context, numDevices, deviceIDs, src,
	     &program);

message("CL Code compiled");

// Create kernel object
cl_kernel kernel;
std::string kernel_name = "convolucion_gpu";
create_kernel(program, kernel_name.c_str(), &kernel);

message("Kernel created");

/* --- Init host data --- */
float *h_input, *h_output, *check_output, *h_filter;
h_input      = (float *) malloc(size_input);
h_output     = (float *) malloc(size_output);
check_output = (float *) malloc(size_output);
h_filter     = (float *) malloc(size_filter);

assert(h_input);
assert(h_output);
assert(check_output);
assert(h_filter);

setup_filter(h_filter, m);
init_input(h_input, n+m);

message("filter and input initialized");

// timespec cpustart, cpuend;
cpu_timer cpu_prof;
cpu_prof.tic();

// clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpustart);
convolucion_cpu(h_input, check_output, h_filter, n, m);
// clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpuend);

cpu_prof.tac();

std::cout << "CPU Profiling: " << cpu_prof.elapsed() << " msecs" << std::endl;

/* --- Allocate buffers --- */
cl_mem d_input, d_output, d_filter;
d_input = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
			 size_input, static_cast<void *>(h_input),
			 &errNum);
d_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
			  size_output, NULL,
			  &errNum);
d_filter = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
			  size_filter, static_cast<void *>(h_filter),
			  &errNum);

clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_input);
clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_output);
clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_filter);  
clSetKernelArg(kernel, 3, sizeof(cl_uint), &n);
clSetKernelArg(kernel, 4, sizeof(cl_uint), &m);

cl_command_queue queue = clCreateCommandQueue(context, deviceIDs[0],
					      // 0, // No Profile
					      CL_QUEUE_PROFILING_ENABLE,
					      &errNum);


message("Command queue created");

// Queue the kernel for execution across the array
// Difference between CUDA and OpenCL: globalWorkSize is the total.
const size_t globalWorkSize[1] = { n };
const size_t localWorkSize[1] = { m };

std::cout << "global work size = " << globalWorkSize[0] << std::endl;
std::cout << "local work size = " << localWorkSize[0] << std::endl;

cl_event prof_event; // Profiling

clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
		       globalWorkSize, localWorkSize,
		       0, NULL, &prof_event);

clFinish(queue); // Profiling: Ensure the queue is finished
clWaitForEvents(1, &prof_event); // Profiling: wait for the event to tic

// Return profiling info
cl_ulong ev_start_time = (cl_ulong) 0;
cl_ulong ev_end_time = (cl_ulong) 0;
size_t ev_size;
clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_QUEUED,
			sizeof(cl_ulong), &ev_start_time, &ev_size);
clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_END,
			sizeof(cl_ulong), &ev_end_time, &ev_size);

double run_time = (double) (ev_end_time - ev_start_time);
std::cout << "Kernel profiling: " << run_time * 1.0e-9 << " seconds";
std::cout << " = " << run_time * 1.0e-6 << " msecs";
std::cout << " = " << run_time << " nanosecs" << std::endl;

errNum = clEnqueueReadBuffer(queue, // command_queue
			     d_output, // buffer
			     CL_TRUE, // blocking_read
			     0, // offset
			     size_output, // cb
			     h_output, // ptr
			     0, NULL, NULL);

if (errNum != CL_SUCCESS){
  std::cout << "Enqueue read buffer error" << std::endl;
  exit(1);
}

std::cout << "Comparison: --------------------" << std::endl;
std::cout << "Displaying differences" << std::endl;
std::cout << "[i] h_output - check_output" << std::endl;
for (int j=0; j < n; j++){
  if (h_output[j] != check_output[j]){
    std::cout << "[" << j << "] " <<
      h_output[j] << " - " << check_output[j] << std::endl;
    // assert(h_output[j] == check_output[j]);
  }
}
message("End comparison");

free(h_input);
free(h_output);
free(check_output);
free(h_filter);

clReleaseMemObject(d_input);
clReleaseMemObject(d_output);
clReleaseMemObject(d_filter);

errNum = clReleaseContext(context);
if (errNum != CL_SUCCESS){
  std::cerr << "Release context error" << std::endl;
}

message("Program ended.");
  return 0;
}
